/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.auth.rest;

import com.cs.cslc.auth.service.AuthService;
import com.cs.cslc.auth.service.UserService;
import com.cs.cslc.auth.vo.LoginUserVO;
import com.cs.cslc.auth.vo.TokenVO;
import com.cs.cslc.auth.vo.VerifyCodePicRes;
import com.cs.cslc.common.constant.HttpStatusConstant;
import com.cs.cslc.common.exception.LoginException;
import com.cs.cslc.common.msg.ObjectResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * AuthController 认证授权.
 * <p>
 * 登陆，登出
 * </p>
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-05-06 19:46
 */
@RestController
@RequestMapping("/auth")
@Tag(name = "认证授权")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    @Operation(summary = "用户登陆，获取token信息")
    public ObjectResult<TokenVO> login(@RequestBody @Validated LoginUserVO loginUserVO) {
        ObjectResult<TokenVO> result = new ObjectResult<>();
        result.setStatus(HttpStatusConstant.FAIL);
        try {
            // 验证码校验
            String verifyCodeMsg = authService.verifyCodeCheck(loginUserVO.getToken(),
                    loginUserVO.getCode());
            // 没有提示信息则进行登录操作
            if (StringUtils.isBlank(verifyCodeMsg)) {
                // 登录操作
                TokenVO tokenVO = new TokenVO();
                tokenVO.setToken(userService.login(loginUserVO.getAccount().trim(), loginUserVO.getPassword().trim()));
                result.setData(tokenVO);
                result.setStatus(HttpStatusConstant.SUCCESS);
            } else {
                result.setMessage(verifyCodeMsg);
            }
        } catch (LoginException e) {
            throw e;
        } catch (Exception e) {
            result.setMessage(e.getMessage());
        }
        return result;
    }

    @PostMapping("/logout")
    @Operation(summary = "登出当前用户")
    public ObjectResult<String> logout() {
        // TODO 清楚缓存token
        return new ObjectResult<>();
    }

    @GetMapping("/login/verifyCode")
    @Operation(summary = "用户登录校验码生成")
    public ObjectResult<VerifyCodePicRes> findVerifyCode() {
        ObjectResult<VerifyCodePicRes> result = new ObjectResult<>();
        // TODO 添加ip限制，防止暴力请求后，把redis内存撑爆
        result.setData(authService.findVerifyCode());
        return result;
    }


}