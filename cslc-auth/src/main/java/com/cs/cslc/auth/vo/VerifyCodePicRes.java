package com.cs.cslc.auth.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * VerifyCodePicRes 验证码响应体.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2021-12-14
 */
@Data
@Schema(description = "验证码响应体")
public class VerifyCodePicRes {

    @Schema(description = "验证码令牌")
    private String token;

    @Schema(description = "图片的base64码")
    private String image;
}
