/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.auth.dto;

import lombok.Data;

/**
 * WxAccessTokenDTO 微信认证token.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-3-9
 */
@Data
public class WxAccessTokenDTO {

    private String access_token;

    private Integer expires_in;

    private String errmsg;
    /**
     * -1	system error	系统繁忙，此时请开发者稍候再试
     * 40001	invalid credential  access_token isinvalid or not latest	获取 access_token 时 AppSecret 错误，或者 access_token 无效。请开发者认真比对 AppSecret 的正确性，或查看是否正在为恰当的公众号调用接口
     * 40013	invalid appid	不合法的 AppID ，请开发者检查 AppID 的正确性，避免异常字符，注意大小写
     * 40002	invalid grant_type	不合法的凭证类型
     * 40125	不合法的 secret	请检查 secret 的正确性，避免异常字符，注意大小写
     * 40164	调用接口的IP地址不在白名单中	请在接口IP白名单中进行设置
     * 41004	appsecret missing	缺少 secret 参数
     * 50004	禁止使用 token 接口
     * 50007	账号已冻结
     * 61024	第三方平台 API 需要使用第三方平台专用 token
     * 40243	AppSecret已被冻结，请登录小程序平台解冻后再次调用。
     */
    private Integer errcode;
}
