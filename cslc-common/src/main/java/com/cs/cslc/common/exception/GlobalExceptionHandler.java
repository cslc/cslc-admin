/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.common.exception;


import com.cs.cslc.common.constant.HttpStatusConstant;
import com.cs.cslc.common.msg.BaseResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * ExceptionHandler 全局异常处理起.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-25 17:05
 */
@RestControllerAdvice("com.cs.cslc")
public class GlobalExceptionHandler {
    /**
     * 访问权限异常
     * 非法访问没有授权的接口
     */
    @ExceptionHandler(value = AuthException.class)
    public BaseResponse authenticationHandler(HttpServletRequest request, HttpServletResponse response, AuthException e) {
        response.setStatus(HttpStatusConstant.AUTHORITY_FAIL);
        return new BaseResponse(e.getStatus(), e.getMessage());
    }

    /**
     * 登录异常
     * 登录错误，token过期时返回
     */
    @ExceptionHandler(value = LoginException.class)
    public BaseResponse loginExceptionHandler(HttpServletRequest request, HttpServletResponse response, LoginException e) {
        response.setStatus(HttpStatusConstant.LOGIN_AUTHORITY_FAIL);
        return new BaseResponse(e.getStatus(), e.getMessage());
    }

    /**
     * 业务异常
     * 返回请求成功的状态，前端通过业务状态status做逻辑处理
     */
    @ExceptionHandler(value = BusinessException.class)
    public BaseResponse businessExceptionHandler(HttpServletRequest request, HttpServletResponse response, BusinessException e) {
        response.setStatus(HttpStatusConstant.SUCCESS);
        return new BaseResponse(HttpStatusConstant.FAIL, e.getMessage());
    }

    /**
     * 表达校验拦截
     * 前端做表单校验，访问到后端的校验为非法访问，所以直接拦截返回500的异常
     */
    @ExceptionHandler(value = BindException.class)
    public BaseResponse bindExceptionHandler(HttpServletRequest request, HttpServletResponse response, BindException e) {
        response.setStatus(HttpStatusConstant.FAIL);
        BindingResult bindingResult = e.getBindingResult();
        List<ObjectError> objectErrorList = bindingResult.getAllErrors();
        ObjectError objectError = objectErrorList.get(0);
        return new BaseResponse(HttpStatusConstant.FAIL, objectError.getDefaultMessage());
    }
}