package com.cs.cslc.common.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * AuthInfoExceptionDTO 异常拦截处理信息对象.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-04-29
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthInfoExceptionDTO {

    public static final String LOGIN_EXCEPTION_TYPE = "login";

    public static final String AUTH_EXCEPTION_TYPE = "auth";

    /**
     * 异常类型：异常类名
     * LoginException、AuthException
     */
    private String exceptionType;
    /**
     * 异常消息
     */
    private String message;
    /**
     * 异常状态
     */
    private Integer statues;
}
