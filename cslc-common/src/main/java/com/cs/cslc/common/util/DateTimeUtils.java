/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.common.util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * DateTimeUtils 日期工具类.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-02 23:23
 */
public class DateTimeUtils {
    /**
     * 模板样式：yyyy-MM-dd HH:mm:ss
     */
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 模板样式：yyyy-MM-dd HH:mm
     */
    public static final String DATETIME_YMDHM_FORMAT = "yyyy-MM-dd HH:mm";

    /**
     * 模板样式：yyyy-MM-dd
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 模板样式：yyyy-MM
     */
    public static final String MONTH_FORMAT = "yyyy-MM";

    /**
     * 模板样式：HH:mm:ss
     */
    public static final String TIME_FORMAT = "HH:mm:ss";

    /**
     * 日期转为字符串
     * <p>
     * 默认格式：yyyy-MM-dd HH:mm:ss
     * </p>
     *
     * @param date 日期
     * @return
     */
    public static String format(LocalDateTime date) {
        return format(date, DATETIME_FORMAT);
    }

    /**
     * 日期转为字符串，安装模板样式转换
     *
     * @param date    日期
     * @param pattern 模板
     * @return
     */
    public static String format(LocalDateTime date, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return date.format(formatter);
    }

    /**
     * 分转毫秒
     *
     * @param minutes 分钟
     * @return 毫秒
     */
    public static long minutesToMilliseconds(int minutes) {
        return minutes * 60 * 1000;
    }

    /**
     * 天数转毫秒
     *
     * @param day 天数
     * @return 毫秒
     */
    public static long dayToMilliseconds(int day) {
        return day * 24L * 60L * 60L * 1000L;
    }
    /**
     * 获得当前月份，
     */
    public static String getMonth(int amount) {
        Calendar time = Calendar.getInstance();
        // 月
        time.add(Calendar.MONTH, amount);
        // 日
        time.set(Calendar.DATE, 1);
        // 时
        time.set(Calendar.HOUR_OF_DAY, 0);
        // 分
        time.set(Calendar.MINUTE, 0);
        // 秒
        time.set(Calendar.SECOND, 0);
        // 毫秒
        time.set(Calendar.MILLISECOND, 0);
        return new SimpleDateFormat(DateTimeUtils.DATETIME_FORMAT).format(time.getTime());
    }

    /**
     * 获得当前月份往前推几个月的，字符串列表
     *
     * <p>
     * getMonthStr(5)
     * [2022-10, 2022-11, 2022-12, 2023-01, 2023-02, 2023-03]
     * </p>
     */
    public static List<String> getMonthStr(int month) {
        List<String> months = new ArrayList<>();
        for (int i = month; i >= 0; i--) {
            Calendar time = Calendar.getInstance();
            // 月
            time.add(Calendar.MONTH, -i);
            // 日
            time.set(Calendar.DATE, 1);
            // 时
            time.set(Calendar.HOUR_OF_DAY, 0);
            // 分
            time.set(Calendar.MINUTE, 0);
            // 秒
            time.set(Calendar.SECOND, 0);
            // 毫秒
            time.set(Calendar.MILLISECOND, 0);
            months.add(new SimpleDateFormat(MONTH_FORMAT).format(time.getTime()));
        }
        return months;
    }
}