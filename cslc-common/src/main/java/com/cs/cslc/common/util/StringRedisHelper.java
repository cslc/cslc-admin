package com.cs.cslc.common.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * StringRedisHelper 字符串redis工具助手类.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2021-12-14
 */
@Component
public class StringRedisHelper {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 存入数据和设置缓存时间
     *
     * @param key     key
     * @param value   value
     * @param timeout 单位秒
     */
    public void set(String key, String value, long timeout) {
        stringRedisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }

    /**
     * 获取数据
     *
     * @param key key
     * @return 匹配对象
     */
    public String get(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * key是否存在
     *
     * @param key key
     * @return ture 存在
     */
    public Boolean hasKey(String key) {
        if (StringUtils.isBlank(key)) {
            return false;
        }
        return stringRedisTemplate.hasKey(key);
    }

}
