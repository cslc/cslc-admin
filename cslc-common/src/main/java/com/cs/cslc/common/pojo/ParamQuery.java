/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.common.pojo;

import com.cs.cslc.common.constant.CommonConstant;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * ParamQuery 查询参数实体.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2018.11.29
 */
@Setter
@Getter
@Tag(name = "查询条件")
public class ParamQuery extends LinkedHashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    /**
     * 默认页码
     */
    @Schema(description = "页码")
    private int page = 1;

    /**
     * 默认每页条数
     */
    @Schema(description = "每页条数")
    private int limit = 10;

    public ParamQuery() {

    }

    public ParamQuery(int page, int limit) {
        this.page = page;
        this.limit = limit;
        if (limit > CommonConstant.LIMIT_MAX) {
            this.limit = CommonConstant.LIMIT_MAX;
        }
    }

    public ParamQuery(Map<String, Object> params) {
        // 设置查询参数
        this.putAll(params);


        // 获取请求参数中的页码
        Object page = params.get(CommonConstant.Page.PAGE);
        if (null != page) {
            this.page = Integer.parseInt(page.toString());
        }

        // 获取请求参数中的页容量，如果超过200则替换为200
        Object limit = params.get(CommonConstant.Page.LIMIT);
        if (null != limit) {
            this.limit = Integer.parseInt(limit.toString());
            if (this.limit > CommonConstant.LIMIT_MAX) {
                this.limit = CommonConstant.LIMIT_MAX;
            }
        }

        // 删除多余的分页查询参数
        this.remove(CommonConstant.Page.PAGE);
        this.remove(CommonConstant.Page.LIMIT);
    }
}