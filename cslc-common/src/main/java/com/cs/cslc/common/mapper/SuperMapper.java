package com.cs.cslc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * BaseMapper 基础Mapper类.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2020-06-06 8:31
 */
public interface SuperMapper<T> extends BaseMapper<T> {

}
