/*
 * Copyright (c) 2020, chenshuai (cs4380@163.com).
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.cs.cslc.common.rest;

import com.cs.cslc.common.biz.BaseBusinessBiz;
import com.cs.cslc.common.constant.HttpStatusConstant;
import com.cs.cslc.common.msg.ObjectResult;
import com.cs.cslc.common.msg.TableResult;
import com.cs.cslc.common.pojo.ParamQuery;
import com.cs.cslc.common.util.StringEscapeEditor;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Map;

/**
 * BaseController 前端控制器基类.
 * <p>
 * 单表操作基础接口，可重写biz层单表操作，满足业务需求
 * </p>
 *
 * @param <Biz>   业务逻辑层
 * @param <Model> 实体
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2020-01-14
 */
@Slf4j
@RestController
public class BaseController<Biz extends BaseBusinessBiz, Model> {

    @Autowired
    protected Biz baseBiz;

    /**
     * 解决类型转换问题
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringEscapeEditor());
        binder.registerCustomEditor(String[].class, new StringEscapeEditor());
    }

    /**
     * 新增单个对象
     *
     * @param entity 对象实体
     */
    @PostMapping
    @Operation(summary = "新增单个对象")
    public ObjectResult<Model> create(@RequestBody @Validated @Parameter(name = "实体对象") Model entity) {
        ObjectResult<Model> result = new ObjectResult<>();

        try {
            baseBiz.insertModel(entity);
            result.setData(entity);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setStatus(HttpStatusConstant.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 删除单个对象
     * <p>
     * 默认物理删除，如果需要可以重写removeById方法实现逻辑删除
     * </p>
     *
     * @param id 主键
     */
    @DeleteMapping("/{id}")
    @Operation(summary = "删除单个对象")
    public ObjectResult<Model> remove(@PathVariable("id") @Parameter(name = "主键") Serializable id) {
        ObjectResult<Model> result = new ObjectResult<>();
        try {
            baseBiz.removeById(id);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setStatus(HttpStatusConstant.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 更新单个对象
     * <p>
     * 根据主键更新属性不为null的值
     * </p>
     *
     * @param id     主键
     * @param entity 实体对象
     */
    @PutMapping("/{id}")
    @Operation(summary = "更新单个对象")
    public ObjectResult<Model> update(@PathVariable("id") @Parameter(name = "主键") Serializable id,
                                      @RequestBody @Validated @Parameter(name = "实体对象") Model entity) {
        ObjectResult<Model> result = new ObjectResult<>();
        if (null == id || StringUtils.isBlank(id.toString())) {
            result.setStatus(HttpStatusConstant.FAIL);
            result.setMessage("请选择待更新的数据！");
            return result;
        }

        try {
            baseBiz.updateById(entity);
            result.setData(entity);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setStatus(HttpStatusConstant.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 通过主键获取对象信息
     *
     * @param id 主键
     */
    @GetMapping("/{id}")
    @Operation(summary = "查询单个对象")
    public ObjectResult<Model> findModelById(@PathVariable("id") @Parameter(name = "主键") Serializable id) {
        ObjectResult<Model> result = new ObjectResult<>();
        try {
            Object obj = baseBiz.getById(id);
            if (null == obj) {
                result.setStatus(HttpStatusConstant.FAIL);
                result.setMessage("该记录已被删除!");
                return result;
            }
            result.setData((Model) obj);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setStatus(HttpStatusConstant.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 分页获取数据列表
     *
     * @param paramQuery 查询条件
     */
    @GetMapping
    @Operation(summary = "分页获取数据列表")
    public TableResult<Model> list(@RequestParam @Parameter(name = "查询条件") Map<String, Object> paramQuery) {
        return baseBiz.selectTableByParamQuery(new ParamQuery(paramQuery));
    }
}