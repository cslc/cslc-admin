package com.cs.cslc.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ApiLimiter api限流注解.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2023-11-06
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiLimiter {
    /**
     * 默认每秒放入桶中的 token。表示每秒钟接口最大调用次数
     */
    double limitNum() default 5;

    /**
     * 表示限流名称，整个工程中需要保证全局唯一。
     */
    String name() default "";
}
