/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.common.enums;

import lombok.Getter;

/**
 * AuthenticationEnums 认证状态码.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-30 14:49
 */
@Getter
public enum AuthenticationEnums {
    /**
     * 访问权限异常，请联系管理员！
     */
    AUTH_FAIL(401001, "访问权限异常，请联系管理员！"),
    /**
     * 用户账号已禁用，请联系管理员！
     */
    DISABLED_ACCOUNT(401002, "用户账号已禁用，请联系管理员！"),
    /**
     * 用户授权过期或者其他端已登出，请重新登陆！
     */
    EXPIRED_TOKEN(403001, "用户授权过期或者其他端已登出，请重新登陆！"),
    /**
     * 用户账号或密码错误！
     */
    UNKNOWN_ACCOUNT(403002, "用户账号或密码错误！");

    /**
     * 编码
     */
    private final int code;
    /**
     * 提示信息
     */
    private final String value;

    AuthenticationEnums(int code, String value) {
        this.code = code;
        this.value = value;
    }
}