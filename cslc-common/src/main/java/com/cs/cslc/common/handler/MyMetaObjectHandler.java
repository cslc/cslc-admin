/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.cs.cslc.common.context.BaseContextHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * MyMetaObjectHandler 自动设置操作人.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2021-06-05 21:25
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        String userId = BaseContextHandler.getUserId();
        String createUserName = BaseContextHandler.getName();
        String tenantId = BaseContextHandler.getTenantId();
        Date now = new Date();
        this.strictInsertFill(metaObject, "createTime", () -> now, Date.class);
        this.strictInsertFill(metaObject, "createUserId", () -> String.valueOf(userId), String.class);
        this.strictInsertFill(metaObject, "createUserName", () -> createUserName, String.class);
        this.strictInsertFill(metaObject, "updateTime", () -> now, Date.class);
        this.strictInsertFill(metaObject, "updateUserId", () -> String.valueOf(userId), String.class);
        this.strictInsertFill(metaObject, "updateUserName", () -> createUserName, String.class);
        this.strictInsertFill(metaObject, "tenantId", () -> tenantId, String.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        String userId = BaseContextHandler.getUserId();
        this.setFieldValByName("updateTime", new Date(), metaObject);
        this.setFieldValByName("updateUserId", String.valueOf(userId), metaObject);
        this.setFieldValByName("updateUserName", BaseContextHandler.getName(), metaObject);
    }
}
