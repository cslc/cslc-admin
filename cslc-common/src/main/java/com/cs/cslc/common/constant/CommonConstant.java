/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.common.constant;

/**
 * CommonConstant 核心常量类.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2018.11.29
 */
public class CommonConstant {
    /**
     * 逻辑值：已处理（删除、禁用等）
     */
    public static final Integer IS_FALSE = 1;

    /**
     * 逻辑值：默认未处理
     */
    public static final Integer IS_TRUE = 0;

    /**
     * 页容量最大值
     * <p>
     * 最大翻页容量，防止数据量过大
     * </p>
     */
    public static final Integer LIMIT_MAX = 200;

    /**
     * 树根节点
     * <p>
     * 树形结构默认根节点标志
     * </p>
     */
    public static final String ROOT = "root";

    /**
     * 标记
     */
    public static final Integer SIGN = 1;

    /**
     * 没有标记
     */
    public static final Integer NOT_SIGN = 0;

    /**
     * 翻页参数常量类
     */
    public class Page {
        /**
         * 页码
         */
        public static final String PAGE = "page";
        /**
         * 页容量（每页数量）
         */
        public static final String LIMIT = "limit";
    }
}