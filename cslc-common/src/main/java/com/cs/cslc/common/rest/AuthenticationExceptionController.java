/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.common.rest;

import com.alibaba.fastjson.JSONObject;
import com.cs.cslc.common.constant.AuthExceptionConstant;
import com.cs.cslc.common.constant.HttpStatusConstant;
import com.cs.cslc.common.exception.AuthException;
import com.cs.cslc.common.exception.BaseException;
import com.cs.cslc.common.exception.LoginException;
import com.cs.cslc.common.pojo.AuthInfoExceptionDTO;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * AuthenticationExceptionController 异常处理转发接口.
 * <p>
 * 解决Shiro自定义拦截器TokenAuthFilter 中无法获取异常的问题
 * </p>
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2021-12-12
 */
@Slf4j
@Controller
public class AuthenticationExceptionController {
    /**
     * 权限认证失败转发请求
     *
     * @param request request
     */
    @RequestMapping(AuthExceptionConstant.AUTH_EXCEPTION_API)
    public void rethrow(HttpServletRequest request) {
        AuthInfoExceptionDTO authInfoExceptionDTO = null;
        try {
            Object info = request.getAttribute(AuthExceptionConstant.AUTH_EXCEPTION_INFO);
            authInfoExceptionDTO = JSONObject.parseObject(info.toString(), AuthInfoExceptionDTO.class);
        } catch (Exception e) {
            log.error("AuthenticationExceptionController", e);
            throw new BaseException("系统内部异常，请联系客服", HttpStatusConstant.AUTHORITY_FAIL);
        }
        if (null != authInfoExceptionDTO) {
            String exceptionType = authInfoExceptionDTO.getExceptionType();
            if (AuthInfoExceptionDTO.AUTH_EXCEPTION_TYPE.equals(exceptionType)) {
                throw new AuthException(authInfoExceptionDTO.getMessage(), authInfoExceptionDTO.getStatues());
            } else {
                throw new LoginException(authInfoExceptionDTO.getMessage(), authInfoExceptionDTO.getStatues());
            }
        }
    }
}