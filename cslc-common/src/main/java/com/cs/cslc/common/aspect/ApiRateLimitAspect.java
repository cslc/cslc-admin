package com.cs.cslc.common.aspect;

import com.cs.cslc.common.annotation.ApiLimiter;
import com.cs.cslc.common.constant.InitialCapacityConstant;
import com.google.common.util.concurrent.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ApiRateLimitAspect api限流aop.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2023-11-06
 */
@Slf4j
@Aspect
@Component
public class ApiRateLimitAspect {
    private static final ConcurrentHashMap<String, RateLimiter> RATE_LIMITER =
            new ConcurrentHashMap<>(InitialCapacityConstant.INITIAL_512_NUMBER);
    private RateLimiter rateLimiter;

    @Pointcut("@annotation(com.cs.cslc.common.annotation.ApiLimiter)")
    public void serviceLimit() {
    }

    @Around("serviceLimit()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        // 获取拦截的方法名
        Signature sig = point.getSignature();
        MethodSignature msig = (MethodSignature) sig;
        // 返回被织入增加处理目标对象
        Object target = point.getTarget();
        // 为了获取注解信息
        Method currentMethod = target.getClass().getMethod(msig.getName(), msig.getParameterTypes());
        // 获取注解信息
        ApiLimiter annotation = currentMethod.getAnnotation(ApiLimiter.class);
        // 获取注解每秒加入桶中的token
        double limitNum = annotation.limitNum();
        // 注解所在方法名区分不同的限流策略
        String functionName = msig.getName();
        if (!RATE_LIMITER.containsKey(functionName)) {
            RATE_LIMITER.put(functionName, RateLimiter.create(limitNum));
        }
        rateLimiter = RATE_LIMITER.get(functionName);
        // 正常放行
        if (rateLimiter.tryAcquire()) {
//            log.info("api 放行");
            return point.proceed();
        }

        log.info("api 限流拦截 触发动作: {} ", functionName);
        return null;
    }
}
