/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.common.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * TreeNodeVO 树节点vo基类.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2018.12.09
 */
@Getter
@Setter
@Schema(description = "树节点基类")
public class TreeNodeVO<T> implements Serializable {

    private static final long serialVersionUID = 6576937181446272996L;

    /**
     * 节点id
     */
    @Schema(description = "节点id")
    protected Object id;

    /**
     * 节点标签
     */
    @Schema(description = "节点标签")
    protected String label;

    /**
     * 父节点id
     */
    @Schema(description = "父节点id")
    protected Object parentId;

    /**
     * 子节点集
     */
    @Schema(description = "子节点集")
    protected List<T> children;


    public TreeNodeVO() {
        children = new ArrayList<>();
    }

    /**
     * 添加节点
     *
     * @param node
     */
    public void add(T node) {
        children.add(node);
    }
}