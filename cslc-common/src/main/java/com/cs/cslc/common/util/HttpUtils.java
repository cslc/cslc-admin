package com.cs.cslc.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

/**
 * HttpUtils 网络请求工具.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2025-5-30
 */
@Slf4j
public class HttpUtils {
    /**
     * 发送json数据的post请求
     * 不支持https
     *
     * @param url      路径
     * @param sendData 参数(json串)
     * @return 返回调接口返回信息
     */
    public static String sendJsonPost(String url, String sendData) {
        String body = "";
        try {
            CloseableHttpClient client = HttpClients.createDefault();

            //创建post方式请求对象
            HttpPost httpPost = new HttpPost(url);

            //装填参数
            StringEntity s = new StringEntity(sendData, Consts.UTF_8);
            s.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                    "application/json"));
            //设置参数到请求对象中
            httpPost.setEntity(s);
            //设置header信息
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

            //执行请求操作，并拿到结果（同步阻塞）
            HttpResponse response = client.execute(httpPost);
            //获取结果实体
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                //按指定编码转换结果实体为String类型
                body = EntityUtils.toString(entity, Consts.UTF_8);
            }
            EntityUtils.consume(entity);
            //为防止频繁调用一个接口导致接口爆掉，每次调用完成后停留100毫秒
            Thread.sleep(100);
        } catch (Exception e) {
            log.error("HttpUtils.sendJsonPost: ", e);
        }
        return body;
    }

    /**
     * 发送json数据的post请求
     * 不支持https
     *
     * @param url      路径
     * @param sendData 参数(json串)
     * @param headers  请求头
     * @return 返回调接口返回信息
     */
    public static String sendJsonPost(String url, String sendData, Header[] headers) {
        String body = "";
        try {
            CloseableHttpClient client = HttpClients.createDefault();

            //创建post方式请求对象
            HttpPost httpPost = new HttpPost(url);

            //装填参数
            StringEntity s = new StringEntity(sendData, Consts.UTF_8);
            s.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                    "application/json"));
            //设置参数到请求对象中
            httpPost.setEntity(s);
            //设置header信息
            httpPost.setHeaders(headers);

            //执行请求操作，并拿到结果（同步阻塞）
            HttpResponse response = client.execute(httpPost);
            if (null == response) {
                log.error("HttpUtils.sendJsonPost: 请求没有response");
                return body;
            }
            //获取结果实体
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                //按指定编码转换结果实体为String类型
                body = EntityUtils.toString(entity, Consts.UTF_8);
            } else {
                log.error("HttpUtils.sendJsonPost: 请求没有响应体");
            }
            EntityUtils.consume(entity);
            //为防止频繁调用一个接口导致接口爆掉，每次调用完成后停留100毫秒
            Thread.sleep(100);
        } catch (Exception e) {
            log.error("HttpUtils.sendJsonPost: ", e);
        }
        return body;
    }

    /**
     * 发送json数据的post请求
     * 不支持https
     *
     * @param url      路径
     * @param sendData params
     * @return 返回调接口返回信息
     */
    public static String sendJsonGet(String url, String sendData) {
        String body = "";
        try {
            CloseableHttpClient client = HttpClients.createDefault();

            //创建get方式请求对象
            HttpGet httpGet = new HttpGet(url + sendData);

            //装填参数
            StringEntity s = new StringEntity(sendData, Consts.UTF_8);
            s.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                    "application/json"));
            //设置header信息
            httpGet.setHeader("Content-type", "application/json");

            //执行请求操作，并拿到结果（同步阻塞）
            HttpResponse response = client.execute(httpGet);
            //获取结果实体
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                //按指定编码转换结果实体为String类型
                body = EntityUtils.toString(entity, Consts.UTF_8);
            }
            EntityUtils.consume(entity);
            //为防止频繁调用一个接口导致接口爆掉，每次调用完成后停留100毫秒
            Thread.sleep(100);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return body;
    }

}
