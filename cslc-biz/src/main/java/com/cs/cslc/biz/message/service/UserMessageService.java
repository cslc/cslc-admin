package com.cs.cslc.biz.message.service;

import com.cs.cslc.admin.entity.BaseMessage;
import com.cs.cslc.common.msg.TableResult;

/**
 * MessageServiceImpl 用户消息服务.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-9-29
 */
public interface UserMessageService {

    TableResult<BaseMessage> page(Integer page, Integer limit, String title);

    BaseMessage findModelById(Integer id);
}
