package com.cs.cslc.biz.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.util.text.BasicTextEncryptor;

/**
 * JasyptUtils 获取配置文件加密工具.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2023-11-03
 */
public class JasyptUtils {
    public static void main(String[] args) {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        // 盐值
        standardPBEStringEncryptor.setPassword("xxxxxxxx");
        // 需要加密的值
        String encryptData = standardPBEStringEncryptor.encrypt("");
        System.out.println(encryptData);

        // 解密后的值
        String decryptData = standardPBEStringEncryptor.decrypt(encryptData);
        System.out.println(decryptData);
    }

}
