package com.cs.cslc.biz.message.rest;

import com.cs.cslc.admin.entity.BaseMessage;
import com.cs.cslc.biz.message.service.UserMessageService;
import com.cs.cslc.common.constant.HttpStatusConstant;
import com.cs.cslc.common.msg.ObjectResult;
import com.cs.cslc.common.msg.TableResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * Message 用户消息服务.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-9-29
 */
@RestController
@RequestMapping("/message")
@Tag(name = "用户消息服务")
public class UserMessageController {

    @Autowired
    private UserMessageService userMessageService;

    @GetMapping("/page")
    @Operation(summary = "分页消息列表")
    public TableResult<BaseMessage> page(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                         @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                         @RequestParam(value = "title", required = false) String title) {
        return userMessageService.page(page, limit, title);
    }

    @GetMapping("/{id}")
    @Operation(summary = "查询消息")
    public ObjectResult<BaseMessage> findModelById(@PathVariable("id") @Parameter(name = "主键") Integer id) {
        ObjectResult<BaseMessage> result = new ObjectResult<>();
        try {
            BaseMessage baseMessage = userMessageService.findModelById(id);
            if (null == baseMessage) {
                result.setStatus(HttpStatusConstant.FAIL);
                result.setMessage("该记录已被删除!");
                return result;
            }
            result.setData(baseMessage);
        } catch (Exception e) {
            result.setStatus(HttpStatusConstant.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

}
