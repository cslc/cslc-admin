/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.biz;

import com.cs.cslc.common.aspect.ApiRateLimitAspect;
import com.cs.cslc.common.exception.GlobalExceptionHandler;
import com.cs.cslc.common.handler.MyMetaObjectHandler;
import com.cs.cslc.common.rest.AuthenticationExceptionController;
import com.cs.cslc.common.util.StringRedisHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * BizBootstrap 业务服务入口.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2018.11.29
 */
@SpringBootApplication
@EnableCaching
@Import(AuthenticationExceptionController.class)
@EnableTransactionManagement
@ComponentScan(
        // 自动设置操作人信息
        basePackageClasses = {MyMetaObjectHandler.class, GlobalExceptionHandler.class, StringRedisHelper.class},
        basePackages = {"com.cs.cslc.biz", "com.cs.cslc.admin", "com.cs.cslc.auth", "com.cs.cslc.tool.oss"})
@MapperScan(basePackages = {"com.cs.cslc.admin.mapper", "com.cs.cslc.biz.*.mapper",
        "com.cs.cslc.tool.oss.*.mapper", "com.cs.cslc.biz.*.*.mapper", "com.cs.cslc.biz.*.*.*.mapper"})
public class BizBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(BizBootstrap.class, args);
    }
}