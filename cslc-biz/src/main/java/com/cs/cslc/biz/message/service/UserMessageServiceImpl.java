package com.cs.cslc.biz.message.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cs.cslc.admin.entity.BaseMessage;
import com.cs.cslc.admin.mapper.BaseMessageMapper;
import com.cs.cslc.common.msg.TableResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * MessageServiceImpl 用户消息服务.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-9-29
 */
@Service
public class UserMessageServiceImpl implements UserMessageService {

    @Autowired
    private BaseMessageMapper baseMessageMapper;

    @Override
    public TableResult<BaseMessage> page(Integer page, Integer limit, String title) {
        // 查询翻页列表数据
        LambdaQueryWrapper<BaseMessage> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(BaseMessage::getId, BaseMessage::getTitle, BaseMessage::getType,
                BaseMessage::getCreateTime);

        if (StringUtils.isNotBlank(title)) {
            queryWrapper.like(BaseMessage::getTitle, title);
        }
        queryWrapper.orderByDesc(BaseMessage::getCreateTime);

        // 获取db记录
        IPage<BaseMessage> iPage = this.baseMessageMapper.selectPage(new Page<>(page, limit), queryWrapper);
        List<BaseMessage> dataList = iPage.getRecords();
        if (null == dataList) {
            return new TableResult<>(0, Collections.emptyList());
        }
        return new TableResult<>(iPage.getTotal(), dataList);
    }

    @Override
    public BaseMessage findModelById(Integer id) {
        return this.baseMessageMapper.selectById(id);
    }
}
