/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.code.generator.utils;

import com.cs.cslc.code.generator.entity.ColumnEntity;
import com.cs.cslc.code.generator.entity.TableEntity;
import com.cs.cslc.common.util.DateTimeUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成器   工具类
 */
public class GeneratorUtils {

    public static List<String> getTemplates() {
        List<String> templates = new ArrayList<>();

        // 前端文件
        templates.add("template/vue/DetailsDialog.vue.vm");
        templates.add("template/vue/EditDetailsDialog.vue.vm");
        templates.add("template/vue/index.vue.vm");
        templates.add("template/vue/index.ts.vm");

        // 后端文件
        templates.add("template/java/mapper.xml.vm");
        templates.add("template/java/biz.java.vm");
        templates.add("template/java/entity.java.vm");
        templates.add("template/java/mapper.java.vm");
        templates.add("template/java/controller.java.vm");

        // db
        templates.add("template/db/init.sql.vm");
        return templates;
    }

    /**
     * db中默认字段
     */
    public static final List<String> DB_DEFAULT_FIELDS = Arrays.asList("is_deleted", "create_time", "create_user_id", "create_user_name", "update_time", "update_user_id", "update_user_name", "tenant_id");

    /**
     * 生成代码
     */
    public static void generatorCode(Map<String, String> table,
                                     List<Map<String, String>> columns, ZipOutputStream zip) {
        //配置信息
        Configuration config = getConfig();

        //表信息
        TableEntity tableEntity = new TableEntity();
        tableEntity.setTableName(table.get("tableName"));
        tableEntity.setComments(table.get("tableComment"));
        //表名转换成Java类名
        String className = tableToJava(tableEntity.getTableName(), config.getString("tablePrefix"));
        tableEntity.setClassNameMax(className);
        tableEntity.setClassNameMin(StringUtils.uncapitalize(className));

        //列信息
        List<ColumnEntity> columsList = new ArrayList<>();
        List<ColumnEntity> vueColumsList = new ArrayList<>();
        for (Map<String, String> column : columns) {
            ColumnEntity columnEntity = new ColumnEntity();
            String columnName = column.get("columnName");
            columnEntity.setColumnName(columnName);
            columnEntity.setDataType(column.get("dataType"));
            columnEntity.setComments(column.get("columnComment"));
            columnEntity.setExtra(column.get("extra"));

            //列名转换成Java属性名
            String attrName = columnToJava(columnEntity.getColumnName());
            columnEntity.setAttrNameMax(attrName);
            columnEntity.setAttrNameMin(StringUtils.uncapitalize(attrName));

            //列的数据类型，转换成Java类型
            String attrType = config.getString(columnEntity.getDataType(), "unknowType");
            columnEntity.setAttrType(attrType);
            // 前端ts数据类型
            if ("Integer".equals(attrType) || "BigDecimal".equals(attrType)) {
                // 整形
                columnEntity.setTsType("number");
            } else {
                // 默认给字符串
                columnEntity.setTsType("string");
            }

            //是否主键
            if ("PRI".equalsIgnoreCase(column.get("columnKey")) && tableEntity.getPk() == null) {
                tableEntity.setPk(columnEntity);
            }
            // 前端id的ts数据类型，不是整形，默认给字符串
            if ("id".equals(columnEntity.getColumnName())) {
                if ("Integer".equals(attrType)) {
                    tableEntity.setTsIdType("number");
                } else {
                    tableEntity.setTsIdType("string");
                }
            }
            columsList.add(columnEntity);

            // vue剔除默认的几个字段
            if (!DB_DEFAULT_FIELDS.contains(columnName)) {
                vueColumsList.add(columnEntity);
            }
        }
        tableEntity.setColumns(columsList);
        tableEntity.setVueColumns(vueColumsList);

        //没主键，则第一个字段为主键
        if (tableEntity.getPk() == null) {
            tableEntity.setPk(tableEntity.getColumns().get(0));
        }

        //设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);

        //封装模板数据
        Map<String, Object> map = new HashMap<>();
        map.put("tableName", tableEntity.getTableName());
        map.put("comments", tableEntity.getComments());
        map.put("pk", tableEntity.getPk());
        map.put("className", tableEntity.getClassNameMax());
        map.put("classname", tableEntity.getClassNameMin());
        map.put("pathName", tableEntity.getClassNameMin().toLowerCase());
        map.put("columns", tableEntity.getColumns());
        map.put("package", config.getString("package"));
        map.put("author", config.getString("author"));
        map.put("datetime", DateTimeUtils.format(LocalDateTime.now(), DateTimeUtils.DATETIME_YMDHM_FORMAT));
        map.put("pojoName", className);


        // vue
        map.put("vueColumns", tableEntity.getVueColumns());
        map.put("tsIdType", tableEntity.getTsIdType());
        map.put("secondModuleName", toLowerCaseFirstOne(className));

        // db 字段
        map.put("uuid", UUID.randomUUID().toString().replace("-", ""));
        map.put("createDate", DateTimeUtils.format(LocalDateTime.now()));

        VelocityContext context = new VelocityContext(map);

        //获取模板列表
        List<String> templates = getTemplates();
        for (String template : templates) {
            //渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);

            try {
                //添加到zip
                zip.putNextEntry(new ZipEntry(getFileName(template, tableEntity.getClassNameMax(), config.getString("package"))));
                IOUtils.write(sw.toString(), zip, "UTF-8");
                IOUtils.closeQuietly(sw);
                zip.closeEntry();
            } catch (IOException e) {
                throw new RuntimeException("渲染模板失败，表名：" + tableEntity.getTableName(), e);
            }
        }
    }

    /**
     * 列名转换成Java属性名
     */
    public static String columnToJava(String columnName) {
        return WordUtils.capitalizeFully(columnName, new char[]{'_'}).replace("_", "");
    }

    /**
     * 表名转换成Java类名
     */
    public static String tableToJava(String tableName, String tablePrefix) {
        if (StringUtils.isNotBlank(tablePrefix)) {
            tableName = tableName.replace(tablePrefix, "");
        }
        return columnToJava(tableName);
    }

    /**
     * 获取配置信息
     */
    public static Configuration getConfig() {
        try {
            return new PropertiesConfiguration("generator.properties");
        } catch (ConfigurationException e) {
            throw new RuntimeException("获取配置文件失败，", e);
        }
    }

    /**
     * 获取文件名
     */
    public static String getFileName(String template, String className, String packageName) {
        String packagePath = "main" + File.separator + "java" + File.separator;
        String frontPath = "ui" + File.separator;
        String dbPath = "db" + File.separator;
        if (StringUtils.isNotBlank(packageName)) {
            packagePath += packageName.replace(".", File.separator) + File.separator;
        }

        // 前端文件
        if (template.contains("EditDetailsDialog.vue.vm")) {
            return frontPath + "views" + File.separator + "biz" + File.separator + toLowerCaseFirstOne(className) + File.separator + "components" + File.separator + "Edit" + className + "DetailsDialog.vue";
        }
        if (template.contains("DetailsDialog.vue.vm")) {
            return frontPath + "views" + File.separator + "biz" + File.separator + toLowerCaseFirstOne(className) + File.separator + "components" + File.separator + className + "DetailsDialog.vue";
        }
        if (template.contains("index.vue.vm")) {
            return frontPath + "views" + File.separator + "biz" + File.separator + toLowerCaseFirstOne(className) + File.separator + "index.vue";
        }
        if (template.contains("index.ts.vm")) {
            return frontPath + "api" + File.separator + "biz" + File.separator + toLowerCaseFirstOne(className) + File.separator + "index.ts";
        }

        // 后端文件
        if (template.contains("biz.java.vm")) {
            return packagePath + toLowerCaseFirstOne(className) + File.separator + "service" + File.separator + className + "ServiceImpl.java";
        }
        if (template.contains("mapper.java.vm")) {
            return packagePath + toLowerCaseFirstOne(className) + File.separator + "mapper" + File.separator + className + "Mapper.java";
        }
        if (template.contains("entity.java.vm")) {
            return packagePath + toLowerCaseFirstOne(className) + File.separator + "entity" + File.separator + className + ".java";
        }
        if (template.contains("controller.java.vm")) {
            return packagePath + toLowerCaseFirstOne(className) + File.separator + "rest" + File.separator + className + "Controller.java";
        }
        if (template.contains("mapper.xml.vm")) {
            return "main" + File.separator + "resources" + File.separator + "mapper" + File.separator + className + File.separator + className + "Mapper.xml";
        }

        // db
        if (template.contains("init.sql.vm")) {
            return dbPath + "init.sql";
        }
        return null;
    }

    /**
     * 首字母转小写
     */
    private static String toLowerCaseFirstOne(String s) {
        if (Character.isLowerCase(s.charAt(0))) {
            return s;
        } else {
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
        }
    }
}