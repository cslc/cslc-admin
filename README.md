# cslc-admin

cslc-admin基础权限单体架构，前后端分离项目   
后端项目基于Spring Boot3、Spring、Spring MVC、MyBatis、MyBatis-plus、Shiro等主流后端技术开发   
前端项目基于 Vue3 + Vite5+ TypeScript5 + Element-Plus + Pinia等主流技术开发

微信: jk_cloud

## 一.系统说明

### 1.目录模块说明

- admin-ui   
  前端项目

- admin服务  
  提供后台管理基础功能的包，供业务项目使用
    - 用户管理管理
    - 组织机构管理
    - 角色权限管理
    - 数据字典管理
    - 系统菜单管理
    - 系统租户管理

- common服务
  提供系统公共基础相关工具的包，供业务项目使用

- auth服务  
  认证授权服务，登陆验证等功能，供业务项目使用

- biz服务（业务服务）  
  业务服务，主程序，做业务系统开发，其他服务打包后注入到此服务（通过pom依赖）

- generator服务 代码生成器，独立项目，可视化页面操作数据库表生成前后端代码包

- doc目录 项目相关资料

### 2.技术架构说明

- 表单验证采用Hiberante-Validator，前端做表单验证，后台只做验证不做提示，能跳过表单验证的都是非正常操作请求，返回错误统一错误提示
- 基础技术栈 Spring、Spring MVC、MyBatis、Shiro
- 插件 Lombok、Swagger、MyBatis-plus、PageHelper
- 数据库 MySql8、Redis6
- jdk17，nodejs20

### 3.代码规范（推荐）

本项目基于 <a href="https://github.com/alibaba/p3c/" target="_blank">阿里代码规范</a>，采用了常见的代码规范

## 二、开发环境搭建

环境搭建此处只做整体思路的说明，具体细节有问题可加群交流沟通

### 1.后端项目环境搭建

#### 1、编辑器插件

- Lombok插件，需要手动下载并安装到编辑器中
- maven插件，一般编辑器都自带，只需要和本地maven关联配置即可

#### 2、maven打包

在当前项目根目录执行，目的下载项目相关依赖，打基础包

```
# 打包当前项目
mvn clean install
```

#### 3、项目配置

- 执行目录中的建表sql脚本（/doc/db/base_db.sql）、初始化数据sql脚本（/doc/db/init_data.sql）
- 配置数据库（mysql、redis），直接调整修改biz服务application.yml中的配置
- 启动服务BizBootstrap，账号admin，密码123456

### 2.前端项目环境搭建

[前端项目admin-ui说明](https://gitee.com/xhbug_cs4380/cslc-admin/tree/master/cslc-admin-ui)

## 三、项目部署

服务器环境jdk17、nginx最新版

### 3.1 后端部署

通过maven打包业务项目，生成cslc-admin-server.jar部署到云服务器。

- 启动后端项目，在jar包同级目录创建启动start.sh脚本，通过linux命令执行
- 关闭后端项目，直接通过linux杀进程命令：kill -9 xxx

```
#!/bin/bash
nohup java -server -Xms128m -Xmx128m -jar ./cslc-admin-server.jar --spring.profiles.active=prod --spring.config.additional-location=./config.yml >./logs/nohup.out 2>&1 &
```

### 3.2 前端部署

在nginx的配置文件中nginx.conf，添加以下配置

```
    server {
        listen       80;
        # 公网域名或者ip地址
        server_name  www.xxxxx.cn;
        location ^~ /api/ {
           # 后端程序地址
           proxy_pass http://localhost:4380;
           proxy_connect_timeout 500s;
           proxy_read_timeout 500s;
           proxy_send_timeout 500s;
           proxy_set_header Host $host;
           proxy_set_header X-Real-IP $remote_addr;
           proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
        
        location / {
            # 前端项目部署目录
            root   /html/cslc-admin-ui/;
            index  index.html index.htm;
        }
    }
```

### 3.3 密码加密

3.3.1 得到加密后的数据

```
public static void main(String[] args) {
    BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
    // 加密所需的salt
    textEncryptor.setPassword("xxxxx");
    // 待配置的加密数据，使用包含 ENC(xxx)
    System.out.println("ENC("+textEncryptor.encrypt("123456")+")");
}
```

3.3.2 添加已下配置

```yaml
jasypt:
  encryptor:
    password: rinxt527c # 自己自定义盐值
    algorithm: PBEWithMD5AndDES
    iv-generator-classname: org.jasypt.iv.NoIvGenerator
```

3.3.3 mysql参考案例

```yaml
spring:
  datasource:
    password: ENC(xxx) # xxx 替换成自己加密后是数据
```

## 四、项目环境推荐文档

### 4.1 本地开发推荐

- [VSCode官网下载地址](https://code.visualstudio.com/)
- [IDEA官网下载地址](https://www.jetbrains.com/idea/download/#section=windows)
- [Nodejs官网下载地址](https://nodejs.org/en/)
- [Maven安装](https://code.visualstudio.com/)

### 4.1 服务器环境推荐

- [Nacos部署](https://blog.csdn.net/cs4380/article/details/106694604)
- [Cenos7安装docker](https://blog.csdn.net/cs4380/article/details/85777416)
- [Docker中安装mysql](https://blog.csdn.net/cs4380/article/details/89928514)
- [Docker中安装redis](https://blog.csdn.net/cs4380/article/details/86582569)
- [Cenos7安装Nginx](https://blog.csdn.net/cs4380/article/details/104551530)

### 4.3 其他推荐

- [OpenJDK17安装](https://blog.csdn.net/cs4380/article/details/97497455) 服务器和本地推荐安装一致的jdk版本
- [IDEA配置外部依赖](https://blog.csdn.net/cs4380/article/details/108937132)
  SpringBoot支持依赖外部文件配置，直接在idea中配置环境变量，不需要手动修改bootstrap.yml

## 五、项目预览图

<table>
    <tr>
        <td><img src="https://gitee.com/xhbug_cs4380/cslc-documentation/raw/master/images/preview/index.png"/></td>
        <td><img src="https://gitee.com/xhbug_cs4380/cslc-documentation/raw/master/images/preview/user_info.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xhbug_cs4380/cslc-documentation/raw/master/images/preview/role.png"/></td>
        <td><img src="https://gitee.com/xhbug_cs4380/cslc-documentation/raw/master/images/preview/menus.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xhbug_cs4380/cslc-documentation/raw/master/images/preview/api.png"/></td>
        <td><img src="https://gitee.com/xhbug_cs4380/cslc-documentation/raw/master/images/preview/user_list.png"/></td>
    </tr>
</table>
