/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.rest;

import com.cs.cslc.admin.entity.SysDictType;
import com.cs.cslc.admin.service.SysDictTypeBiz;
import com.cs.cslc.admin.vo.SysDictTypeTreeVO;
import com.cs.cslc.common.msg.ObjectResult;
import com.cs.cslc.common.rest.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * SysDictType 数据字典类型.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-07 21:41
 */
@RestController
@RequestMapping("/admin/sysDictTypes")
@Tag(name = "数据字典类型")
public class SysDictTypeController extends BaseController<SysDictTypeBiz, SysDictType> {

    @GetMapping("/type/{parentId}")
    @Operation(summary = "通过parentId获取子typeCode匹配集合")
    public ObjectResult<List<SysDictType>> findSonTypeByParentId(@PathVariable("parentId")
                                                                 @Parameter(name = "parentId") String parentId) {
        ObjectResult<List<SysDictType>> result = new ObjectResult<>();
        result.setData(this.baseBiz.findSonTypeByParentId(parentId));
        return result;
    }

    @GetMapping("/tree")
    @Operation(summary = "获取数据字典类型树")
    public ObjectResult<List<SysDictTypeTreeVO>> getDictTypeTree() {
        ObjectResult<List<SysDictTypeTreeVO>> result = new ObjectResult<>();
        result.setData(this.baseBiz.getDictTypeTree());
        return result;
    }

}