/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cs.cslc.admin.entity.BaseUserRole;
import com.cs.cslc.admin.mapper.BaseUserRoleMapper;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import com.cs.cslc.common.util.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * BaseUserRole 用户角色关系表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-11-14 13:50
 */
@Service
public class BaseUserRoleBiz extends BaseBusinessBiz<BaseUserRoleMapper, BaseUserRole> {

    /**
     * 通过用户id，获取用户角色列表
     *
     * @param userId 用户id
     * @return 用户的角色集 List<role>
     */
    public List<String> getRolesByUserId(String userId) {
        List<BaseUserRole> userRoleList = this.getUserRoleByUserId(userId);
        if (BeanUtil.isEmpty(userRoleList)) {
            return new ArrayList<>();
        }
        return userRoleList.stream().map(BaseUserRole::getRoleCode).collect(Collectors.toList());
    }

    /**
     * 通过用户id，获取用户角色关系
     *
     * @param userId 用户id
     * @return 用户角色关系集
     */
    public List<BaseUserRole> getUserRoleByUserId(String userId) {
        if (StringUtils.isBlank(userId)) {
            return new ArrayList<>();
        }
        BaseUserRole baseUserRole = new BaseUserRole();
        baseUserRole.setUserId(userId);
        return this.selectModelList(baseUserRole);
    }

    /**
     * 删除指定用户的角色关系
     *
     * @param userId 用户主键
     */
    public void delUserRoleByRoleCodes(String userId) {
        this.delUserRoleByRoleCodes(userId, null);
    }

    /**
     * 通过用户主键或者用户编码集合删除
     *
     * @param userId    用户主键
     * @param roleCodes 角色编码集
     */
    public void delUserRoleByRoleCodes(String userId, List<String> roleCodes) {
        LambdaQueryWrapper<BaseUserRole> queryWrapper = Wrappers.lambdaQuery();
        // 判断是否存在角色编码
        if (BeanUtil.isNotEmpty(roleCodes)) {
            queryWrapper.in(BaseUserRole::getRoleCode, roleCodes);
        }
        // 判断是否指定用户
        if (StringUtils.isNotBlank(userId)) {
            queryWrapper.eq(BaseUserRole::getUserId, userId);
        }
        this.remove(queryWrapper);
    }

    /**
     * 添加用户角色关系
     *
     * @param userId    用户主键
     * @param roleCodes 角色编码集
     */
    public void addUserRole(String userId, List<String> roleCodes) {
        if (BeanUtil.isEmpty(roleCodes)) {
            return;
        }
        for (String role : roleCodes) {
            BaseUserRole userRole = new BaseUserRole();
            userRole.setRoleCode(role);
            userRole.setUserId(userId);
            this.baseMapper.insert(userRole);
        }
    }

}