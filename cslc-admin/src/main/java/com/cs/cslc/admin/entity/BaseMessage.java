package com.cs.cslc.admin.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Message 系统消息.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2023-10-24
 */
@Getter
@Setter
@TableName("base_message")
public class BaseMessage implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 消息标题
     */
    @TableField("title")
    private String title;
    /**
     * 消息类型
     */
    @TableField("type")
    private String type;
    /**
     * 消息内容
     */
    @TableField("content")
    private String content;
    /**
     * 是否删除(1:删除|0:未删除)
     */
    @TableLogic
    @Schema(description = "是否删除")
    @TableField("is_deleted")
    private Integer isDeleted;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建日期")
    private Date createTime;
    /**
     * 创建用户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建用户Id")
    private String createUserId;
    /**
     * 创建用户姓名
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建用户姓名")
    private String createUserName;
    /**
     * 最后更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新日期")
    private Date updateTime;
    /**
     * 最后更新用户Id
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新用户Id")
    private String updateUserId;
    /**
     * 最后更新用户姓名
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新用户姓名")
    private String updateUserName;
    /**
     * 租户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "租户Id")
    private String tenantId;
}