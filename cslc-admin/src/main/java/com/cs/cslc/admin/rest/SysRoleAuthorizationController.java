/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.rest;

import com.cs.cslc.admin.entity.SysRoleAuthorization;
import com.cs.cslc.admin.service.SysRoleAuthorizationBiz;
import com.cs.cslc.admin.vo.RoleAuthorizationVO;
import com.cs.cslc.common.constant.MessageConstant;
import com.cs.cslc.common.enums.HttpStatusEnums;
import com.cs.cslc.common.msg.ObjectResult;
import com.cs.cslc.common.rest.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * SysRoleAuthorization 系统角色授权关系表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-11-13 10:43
 */
@RestController
@RequestMapping("/admin/sysRoleAuthorizations")
@Tag(name = "系统角色授权关系表")
public class SysRoleAuthorizationController extends BaseController<SysRoleAuthorizationBiz, SysRoleAuthorization> {
    /**
     * 角色授权接口，角色和菜单做关联
     *
     * @param roleCode    角色编码
     * @param roleMenusVO 角色菜单
     * @return
     */
    @PostMapping("/{roleCode}")
    @Operation(summary = "角色授权接口，角色和菜单做关联")
    public ObjectResult setRoleAuth(
            @PathVariable("roleCode") @Parameter(name = "角色编码") String roleCode,
            @RequestBody @Parameter(name = "角色权限") RoleAuthorizationVO roleMenusVO) {
        ObjectResult result = new ObjectResult();
        if (StringUtils.isBlank(roleCode)) {
            result.setStatus(HttpStatusEnums.FAIL.getCode());
            result.setMessage(MessageConstant.MVC_VALIDATE_MSG);
            return result;
        }
        this.baseBiz.setRoleAuth(roleCode, roleMenusVO);
        return result;
    }

    /**
     * 获取角色对应的菜单权限id
     *
     * @param roleCode 角色编码
     * @return
     */
    @GetMapping("/{roleCode}/resourceType/{resourceType}")
    @Operation(summary = "获取角色对应的权限集合")
    public ObjectResult<List<String>> getAuthIdByRoleCode(
            @PathVariable("roleCode") @Parameter(name = "角色编码") String roleCode,
            @PathVariable("resourceType") @Parameter(name = "权限类型") Integer resourceType) {
        ObjectResult<List<String>> result = new ObjectResult<>();
        if (StringUtils.isBlank(roleCode) || null == resourceType) {
            result.setStatus(HttpStatusEnums.FAIL.getCode());
            result.setMessage("请选择角色，查看授权信息！");
            return result;
        }
        result.setData(this.baseBiz.getAuthIdByRoleCode(roleCode, resourceType));
        return result;
    }

    /**
     * 获取角色对应的菜单权限集合
     *
     * @param roleCode 角色编码
     * @return
     */
    @GetMapping("/roleCode/{roleCode}/menus")
    @Operation(summary = "获取角色对应的菜单权限集合")
    public ObjectResult<List<String>> getMenuAuthByRoleCode(
            @PathVariable("roleCode") @Parameter(name = "角色编码") String roleCode) {
        ObjectResult<List<String>> result = new ObjectResult<>();
        if (StringUtils.isBlank(roleCode)) {
            result.setStatus(HttpStatusEnums.FAIL.getCode());
            result.setMessage("请选择角色，查看授权信息！");
            return result;
        }
        result.setData(this.baseBiz.getMenuAuthByRoleCode(roleCode));
        return result;
    }
}