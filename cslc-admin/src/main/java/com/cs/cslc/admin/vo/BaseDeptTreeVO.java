/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.vo;

import com.cs.cslc.admin.entity.BaseDept;
import com.cs.cslc.common.vo.TreeNodeVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;

import java.util.Date;

/**
 * BaseDeptTreeVO 部门树.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-12-09 14:02
 */
@Getter
@Schema(description = "部门树")
public class BaseDeptTreeVO extends TreeNodeVO<BaseDept> {
    /**
     * 部门编码
     */
    @Schema(description = "部门编码")
    private String deptCode;

    /**
     * 部门名称
     */
    @Schema(description = "部门名称")
    private String deptName;

    /**
     * 最后更新日期
     */
    @Schema(description = "最后更新日期")
    private Date updateTime;

    /**
     * 创建日期
     */
    @Schema(description = "创建日期")
    private Date createTime;

    /**
     * 构造部门树
     *
     * @param baseDept 部门信息
     */
    public BaseDeptTreeVO(BaseDept baseDept) {
        this.id = baseDept.getId();
        this.parentId = baseDept.getParentId();
        this.deptCode = baseDept.getDeptCode();
        this.deptName = baseDept.getDeptName();
        this.updateTime = baseDept.getUpdateTime();
        this.createTime = baseDept.getCreateTime();
    }
}