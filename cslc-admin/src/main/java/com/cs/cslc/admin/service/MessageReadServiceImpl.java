package com.cs.cslc.admin.service;

import com.cs.cslc.admin.entity.BaseMessageRead;
import com.cs.cslc.admin.mapper.BaseMessageReadMapper;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import org.springframework.stereotype.Service;

/**
 * MessageRead 消息读取记录表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2022-11-28 15:01
 */
@Service
public class MessageReadServiceImpl extends BaseBusinessBiz<BaseMessageReadMapper, BaseMessageRead> {
}