/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.vo;

import com.cs.cslc.common.vo.TreeNodeVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * SysDictValue 数据字典类型VO.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-07 21:41
 */
@Setter
@Getter
@Schema(description = "数据字典类型VO")
public class SysDictTypeTreeVO extends TreeNodeVO<SysDictTypeTreeVO> {

    /**
     * 是否展开
     */
    @Schema(description = "是否展开")
    private boolean expand;

    /**
     * 字典类型code
     */
    @Schema(description = "字典类型code")
    private String dictTypeCode;

    /**
     * 初始化节点，默认为展开
     *
     * @param id           节点id
     * @param parentId     父节点id
     * @param label        节点标签
     * @param dictTypeCode 字典类型code
     */
    public SysDictTypeTreeVO(Object id, Object parentId, String label, String dictTypeCode) {
        this.id = id;
        this.parentId = parentId;
        this.label = label;
        this.dictTypeCode = dictTypeCode;
    }

    /**
     * 初始化节点
     *
     * @param id           节点id
     * @param parentId     父节点id
     * @param label        节点标签
     * @param dictTypeCode 字典类型code
     * @param expand       是否展开
     */
    public SysDictTypeTreeVO(Object id, Object parentId, String label, String dictTypeCode, boolean expand) {
        this.id = id;
        this.parentId = parentId;
        this.label = label;
        this.dictTypeCode = dictTypeCode;
        this.expand = expand;
    }


}