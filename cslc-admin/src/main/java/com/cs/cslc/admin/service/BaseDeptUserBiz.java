/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cs.cslc.admin.entity.BaseDeptUser;
import com.cs.cslc.admin.mapper.BaseDeptUserMapper;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * BaseDeptUser 基础角色部门关系表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-12-06 09:14
 */
@Service
public class BaseDeptUserBiz extends BaseBusinessBiz<BaseDeptUserMapper, BaseDeptUser> {

    /**
     * 删除部门用户关系
     *
     * @param deptUser 部门用户
     */
    public void delDeptUser(BaseDeptUser deptUser) {
        LambdaQueryWrapper<BaseDeptUser> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(BaseDeptUser::getUserId, deptUser.getUserId());
        queryWrapper.eq(BaseDeptUser::getDeptCode, deptUser.getDeptCode());
        this.baseMapper.delete(queryWrapper);
    }

    /**
     * 查询用户部门编码集合
     *
     * @param userId 用户主键
     * @return 用户的部门关系
     */
    public List<BaseDeptUser> getDeptUserByUserId(String userId) {
        if (StringUtils.isBlank(userId)) {
            return new ArrayList<>();
        }
        LambdaQueryWrapper<BaseDeptUser> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(BaseDeptUser::getUserId, userId);
        return this.baseMapper.selectList(queryWrapper);
    }

    /**
     * 查询用户部门编码集合
     *
     * @param userId 用户主键
     * @return 用户的部门编码集
     */
    public List<String> getDeptCodesByUserId(String userId) {
        List<String> codes = new ArrayList<>();
        List<BaseDeptUser> deptUserList = this.getDeptUserByUserId(userId);
        for (BaseDeptUser baseDeptUser : deptUserList) {
            codes.add(baseDeptUser.getDeptCode());
        }
        return codes;
    }

    /**
     * 查询不同存在的用户个数
     *
     * @param deptCode 部门编码
     */
    public Long getDeptSizeByDeptCode(String deptCode) {
        if (StringUtils.isBlank(deptCode)) {
            return 0L;
        }
        LambdaQueryWrapper<BaseDeptUser> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(BaseDeptUser::getDeptCode, deptCode);
        return this.baseMapper.selectCount(queryWrapper);
    }
}