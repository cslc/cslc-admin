/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.service;

import com.cs.cslc.admin.dto.MenuTreeDTO;
import com.cs.cslc.admin.entity.SysMenu;
import com.cs.cslc.admin.mapper.SysMenuMapper;
import com.cs.cslc.admin.vo.SysMenuTreeVO;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import com.cs.cslc.common.constant.CommonConstant;
import com.cs.cslc.common.constant.HttpStatusConstant;
import com.cs.cslc.common.constant.InitialCapacityConstant;
import com.cs.cslc.common.exception.BusinessException;
import com.cs.cslc.common.pojo.ParamQuery;
import com.cs.cslc.common.util.BeanUtil;
import com.cs.cslc.common.util.TreeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * SysMenu 系统基础菜单表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-11 19:51
 */
@Service
public class SysMenuBiz extends BaseBusinessBiz<SysMenuMapper, SysMenu> {

    @Override
    public boolean insertModel(SysMenu entity) {
        Long count = this.getCountByMenuCode(entity.getMenuCode());
        if (count > 0) {
            throw new BusinessException("菜单编码已存在，请重新输入！");
        }

        return super.insertModel(entity);
    }

    @Override
    public boolean removeById(Serializable id) {
        if (null == id) {
            throw new BusinessException("请选择待删除对象！", HttpStatusConstant.FAIL);
        }
        Long sons = this.getSonCountByMenuId(id.toString());
        if (sons > 0) {
            throw new BusinessException("请先删除当前菜单的子菜单！", HttpStatusConstant.FAIL);
        }
        SysMenu sysMenu = super.getById(id);
        if (null == sysMenu) {
            throw new BusinessException("删除的菜单不存在", HttpStatusConstant.FAIL);
        }
        // 重置code，防止code唯一索引
        sysMenu.setMenuCode(sysMenu.getId() + "_" + sysMenu.getMenuCode());
        super.updateById(sysMenu);

        // 逻辑删除菜单，菜单对应的按钮关系、权限关系保留
        return super.removeById(sysMenu);
    }

    @Override
    public List<SysMenu> selectListAll(ParamQuery query) {
        String[] fields = {"id", "parent_id", "menu_title", "menu_type", "menu_path", "order_num", "menu_code"};
        List<SysMenu> dataList = super.selectFieldListAllAsc(query, fields, "order_num");
        return BeanUtil.isNotEmpty(dataList) ? dataList : new ArrayList<>();
    }

    /**
     * 获取全部菜单树
     * <p>
     * 获取系统全部菜单数据，排除已删除的
     * </p>
     *
     * @return 匹配数据
     */
    public List<SysMenuTreeVO> getSysMenuTree() {
        // 获取全部数据
        List<SysMenu> menuList = this.selectListAll(null);
        return this.getSysMenuTree(menuList);
    }

    /**
     * 通过菜单列表构建菜单树
     *
     * @param menuList 菜单列表
     * @return 匹配数据
     */
    public List<SysMenuTreeVO> getSysMenuTree(List<SysMenu> menuList) {
        // 缓存树型结构数据
        List<SysMenuTreeVO> tree = new ArrayList<>(InitialCapacityConstant.INITIAL_128_NUMBER);
        // 构造数据
        if (BeanUtil.isNotEmpty(menuList)) {
            for (SysMenu sysMenu : menuList) {
                // 根节点展开
                tree.add(new SysMenuTreeVO(sysMenu));
            }
        }
        List<SysMenuTreeVO> result = TreeUtil.buildByRecursive(tree, CommonConstant.ROOT);
        return BeanUtil.isEmpty(result) ? new ArrayList<>() : result;
    }

    /**
     * 通过菜单编码获取菜单树
     *
     * @param menuCode 菜单编码
     * @return 个数
     */
    public Long getCountByMenuCode(String menuCode) {
        if (StringUtils.isBlank(menuCode)) {
            return 0L;
        }
        SysMenu sysMenu = new SysMenu();
        sysMenu.setMenuCode(menuCode);
        sysMenu.setIsDeleted(CommonConstant.IS_FALSE);
        return super.selectCount(sysMenu);
    }

    /**
     * 通过父级菜单id，统计子集个数
     *
     * @param parentId 父级菜单id
     * @return 个数
     */
    public Long getSonCountByMenuId(String parentId) {
        if (StringUtils.isBlank(parentId)) {
            return 0L;
        }
        SysMenu sysMenu = new SysMenu();
        sysMenu.setParentId(parentId);
        sysMenu.setIsDeleted(CommonConstant.IS_FALSE);
        return this.selectCount(sysMenu);
    }

    /**
     * 通过菜单ids，获取菜单列表
     *
     * @param menuIds 菜单ids
     * @return 匹配数据
     */
    public List<SysMenu> getMenuListByMenuIds(List<String> menuIds) {
        if (BeanUtil.isEmpty(menuIds)) {
            return new ArrayList<>();
        }
        List<SysMenu> sysMenuList = this.baseMapper.selectMenuListByMenuIds(menuIds);
        return BeanUtil.isEmpty(sysMenuList) ? new ArrayList<>() : sysMenuList;
    }

    /**
     * 获取所有菜单列表
     *
     * @return 匹配数据
     */
    public List<SysMenu> getAllMenuList() {
        List<SysMenu> sysMenuList = this.baseMapper.selectMenuListByMenuIds(null);
        return BeanUtil.isEmpty(sysMenuList) ? new ArrayList<>() : sysMenuList;
    }

    /**
     * 通过菜单列表构建菜单树
     * <p>
     * 前端动态路由树
     * </p>
     *
     * @param menuList 菜单列表
     * @return 匹配数据
     */
    public List<MenuTreeDTO> getMenuTreeSysMenuTree(List<SysMenu> menuList) {
        // 缓存树型结构数据
        List<MenuTreeDTO> tree = new ArrayList<>(InitialCapacityConstant.INITIAL_128_NUMBER);
        // 构造数据
        if (BeanUtil.isNotEmpty(menuList)) {
            for (SysMenu sysMenu : menuList) {
                tree.add(new MenuTreeDTO(sysMenu));
            }
        }
        List<MenuTreeDTO> result = TreeUtil.buildByRecursive(tree, CommonConstant.ROOT);
        return BeanUtil.isEmpty(result) ? new ArrayList<>() : result;
    }
}