package com.cs.cslc.admin.mapper;

import com.cs.cslc.admin.entity.BaseMessage;
import com.cs.cslc.common.mapper.SuperMapper;

/**
 * Message 系统消息表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2022-11-28 15:01
 */
public interface BaseMessageMapper extends SuperMapper<BaseMessage> {

}
