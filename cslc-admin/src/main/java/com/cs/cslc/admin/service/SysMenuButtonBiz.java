/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cs.cslc.admin.dto.MenuButtonCodeDTO;
import com.cs.cslc.admin.entity.SysMenuButton;
import com.cs.cslc.admin.mapper.SysMenuButtonMapper;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import com.cs.cslc.common.constant.CommonConstant;
import com.cs.cslc.common.constant.HttpStatusConstant;
import com.cs.cslc.common.exception.BusinessException;
import com.cs.cslc.common.msg.TableResult;
import com.cs.cslc.common.pojo.ParamQuery;
import com.cs.cslc.common.util.BeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * SysMenuButton 系统基础菜单按钮表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-11-12 10:22
 */
@Service
public class SysMenuButtonBiz extends BaseBusinessBiz<SysMenuButtonMapper, SysMenuButton> {

    @Override
    public boolean insertModel(SysMenuButton entity) {
        Long count = this.getCountByButtonCode(entity.getButtonCode());
        if (count > 0) {
            throw new BusinessException("按钮编码已存在，请重新输入！");
        }
        return super.insertModel(entity);
    }

    /**
     * 通过按钮编码统计个数
     *
     * @param buttonCode 菜单编码
     * @return 个数
     */
    public Long getCountByButtonCode(String buttonCode) {
        if (StringUtils.isBlank(buttonCode)) {
            return 0L;
        }
        SysMenuButton sysMenuButton = new SysMenuButton();
        sysMenuButton.setButtonCode(buttonCode);
        return super.selectCount(sysMenuButton);
    }

    @Override
    public boolean removeById(Serializable id) {
        if (null == id) {
            throw new BusinessException("请选择待删除对象！", HttpStatusConstant.FAIL);
        }
        SysMenuButton sysMenuButton = super.getById(id);
        if (null == sysMenuButton) {
            throw new BusinessException("删除的按钮不存在", HttpStatusConstant.FAIL);
        }
        // 重置code，防止code唯一索引
        sysMenuButton.setButtonCode(sysMenuButton.getId() + "_" + sysMenuButton.getButtonCode());
        super.updateById(sysMenuButton);

        // 逻辑删除按钮
        return super.removeById(sysMenuButton);
    }

    @Override
    public TableResult<SysMenuButton> selectTableByParamQuery(ParamQuery query) {
        String[] fields = new String[]{"id", "button_title", "button_code", "url", "method", "description"};
        query.setLimit(CommonConstant.LIMIT_MAX);
        return this.selectTableByParamQuery(query, fields);
    }

    /**
     * 通过主键id获取菜单按钮关系列表
     *
     * @param menuButtonIds 主键ids
     * @return 匹配数据
     */
    public List<MenuButtonCodeDTO> getMenuCodeAndButCodeByButIds(List<Integer> menuButtonIds) {
        if (BeanUtil.isEmpty(menuButtonIds)) {
            return new ArrayList<>();
        }
        List<MenuButtonCodeDTO> menuButtonList = this.baseMapper.selectMenuCodeAndButCodeByButIds(menuButtonIds);
        return BeanUtil.isEmpty(menuButtonList) ? new ArrayList<>() : menuButtonList;
    }

    /**
     * 获取所有菜单按钮关系
     *
     * @return 匹配数据
     */
    public List<MenuButtonCodeDTO> getAllMenuCodeAndButCode() {
        List<MenuButtonCodeDTO> menuButtonList = this.baseMapper.selectMenuCodeAndButCodeByButIds(null);
        return BeanUtil.isEmpty(menuButtonList) ? new ArrayList<>() : menuButtonList;
    }

    /**
     * 通过菜单按钮主键ids获取列表
     *
     * @param buttonIds 菜单按钮ids
     * @return "id", "buttonTitle", "buttonCode", "url", "method"
     */
    public List<SysMenuButton> getMenuButtonByIds(List<Integer> buttonIds) {
        LambdaQueryWrapper<SysMenuButton> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(SysMenuButton::getId, SysMenuButton::getButtonTitle, SysMenuButton::getButtonCode, SysMenuButton::getUrl
                , SysMenuButton::getMethod);
        queryWrapper.in(SysMenuButton::getId, buttonIds);
        return this.baseMapper.selectList(queryWrapper);
    }
}