/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.cs.cslc.common.validate.AddField;
import com.cs.cslc.common.validate.UpdateField;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * SysMenuButton 系统基础菜单按钮表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-11-12 10:22
 */
@Setter
@Getter
@TableName("sys_menu_button")
@Schema(title = "系统基础菜单按钮")
public class SysMenuButton implements Serializable {

    private static final long serialVersionUID = -5236506423780455951L;

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    @Schema(title = "主键")
    @NotNull(groups = UpdateField.class)
    private Integer id;

    /**
     * 菜单id
     */
    @Schema(title = "菜单id")
    @NotBlank(groups = {AddField.class, UpdateField.class})
    @Size(max = 64, groups = {AddField.class, UpdateField.class})
    @TableField("menu_id")
    private String menuId;

    /**
     * 按钮编码
     */
    @Schema(title = "按钮编码")
    @NotBlank(groups = {AddField.class, UpdateField.class})
    @Size(max = 128, groups = {AddField.class, UpdateField.class})
    @TableField("button_code")
    private String buttonCode;

    /**
     * 按钮标题
     */
    @Schema(title = "按钮标题")
    @NotBlank(groups = {AddField.class, UpdateField.class})
    @Size(max = 64, groups = {AddField.class, UpdateField.class})
    @TableField("button_title")
    private String buttonTitle;

    /**
     * 请求地址
     */
    @Schema(title = "请求地址")
    @NotBlank(groups = {AddField.class, UpdateField.class})
    @Size(max = 256, groups = {AddField.class, UpdateField.class})
    @TableField("url")
    private String url;

    /**
     * 请求方式（POST|GET|DELETE|PUT）
     */
    @Schema(title = "请求方式（POST|GET|DELETE|PUT）")
    @NotBlank(groups = {AddField.class, UpdateField.class})
    @Size(max = 12, groups = {AddField.class, UpdateField.class})
    @TableField("method")
    private String method;

    /**
     * 菜单描述
     */
    @Schema(title = "菜单描述")
    @TableField("description")
    private String description;

    /**
     * 是否删除(1:删除|0:未删除)
     */
    @TableLogic
    @Schema(title = "是否删除")
    @TableField("is_deleted")
    private Integer isDeleted;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(title = "创建日期")
    private Date createTime;
    /**
     * 创建用户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(title = "创建用户Id")
    private String createUserId;
    /**
     * 创建用户姓名
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(title = "创建用户姓名")
    private String createUserName;
    /**
     * 最后更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(title = "最后更新日期")
    private Date updateTime;
    /**
     * 最后更新用户Id
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(title = "最后更新用户Id")
    private String updateUserId;
    /**
     * 最后更新用户姓名
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(title = "最后更新用户姓名")
    private String updateUserName;
    /**
     * 租户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(title = "租户Id")
    private String tenantId;
}