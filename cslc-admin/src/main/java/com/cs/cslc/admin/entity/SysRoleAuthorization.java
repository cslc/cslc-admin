/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * SysRoleAuthorization 系统角色授权关系表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-11-13 10:43
 */
@Getter
@Setter
@TableName("sys_role_authorization")
@Schema(description = "系统角色授权关系")
public class SysRoleAuthorization implements Serializable {

    /**
     * 资源类型: 菜单
     */
    public static final Integer MENU_RESOURCE_TYPE = 0;
    /**
     * 资源类型: 按钮
     */
    public static final Integer BUTTON_RESOURCE_TYPE = 1;

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    @Schema(description = "主键")
    private Integer id;

    /**
     * 角色编码
     */
    @Schema(description = "角色编码")
    @TableField("role_code")
    private String roleCode;

    /**
     * 资源id
     */
    @Schema(description = "资源id")
    @TableField("resource_id")
    private String resourceId;

    /**
     * 菜单按钮对应的菜单编码
     */
    @Schema(description = "菜单按钮对应的菜单编码")
    @TableField("menu_code")
    private String menuCode;

    /**
     * 资源类型（0菜单|1按钮）
     */
    @Schema(description = "资源类型（0菜单|1按钮）")
    @TableField("resource_type")
    private Integer resourceType;

    /**
     * 租户Id
     */
    @Schema(description = "租户Id")
    @TableField("tenant_id")
    private String tenantId;
}