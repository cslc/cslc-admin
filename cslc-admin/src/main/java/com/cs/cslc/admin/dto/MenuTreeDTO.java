/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.dto;

import com.cs.cslc.admin.entity.SysMenu;
import com.cs.cslc.common.vo.TreeNodeVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * MenuTreeDTO 系统菜单树.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-19 11:40
 */
@Getter
@Setter
@Schema(title = "系统菜单树")
public class MenuTreeDTO extends TreeNodeVO<SysMenu> {

    /**
     * 菜单类型(0:目录|1:菜单)
     */
    @Schema(title = "菜单类型")
    private Integer type;

    /**
     * 菜单编码
     */
    @Schema(title = "菜单编码")
    private String menuCode;
    /**
     * 菜单名称
     */
    @Schema(title = "菜单名称")
    private String name;

    /**
     * 菜单路径（路径别名）
     */
    @Schema(title = "菜单路径（路径别名）")
    private String path;

    /**
     * 菜单组件地址(菜单url地址)
     */
    @Schema(title = "菜单组件地址(菜单url地址)")
    private String component;

    /**
     * 页面组件(页面目录地址)
     */
    @Schema(title = "页面组件(页面目录地址)")
    private String redirect;

    /**
     * 是否隐藏菜单
     */
    @Schema(title = "是否隐藏菜单")
    private Boolean hidden;

    /**
     * meta
     */
    @Schema(title = "菜单Meta信息")
    private MenuMetaDTO meta;

    /**
     * 构建菜单树
     *
     * @param sysMenu 菜单基本信息
     */
    public MenuTreeDTO(SysMenu sysMenu) {
        this.id = sysMenu.getId();
        this.parentId = sysMenu.getParentId();
        this.type = sysMenu.getMenuType();
        this.path = sysMenu.getMenuPath();
        this.name = sysMenu.getMenuCode();
        this.menuCode = sysMenu.getMenuCode();
        this.redirect = sysMenu.getRedirect();
        this.component = sysMenu.getComponent();
        meta = MenuMetaDTO.builder()
                .title(sysMenu.getMenuTitle())
                .icon(sysMenu.getMenuIcon())
                // false 显示，true隐藏
                .hidden(0 != sysMenu.getHidden())
                .build();
    }
}