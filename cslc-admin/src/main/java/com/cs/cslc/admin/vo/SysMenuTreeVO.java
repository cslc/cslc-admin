/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.vo;

import com.cs.cslc.admin.entity.SysMenu;
import com.cs.cslc.common.vo.TreeNodeVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * SysMenuTreeVO 系统菜单树.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-19 11:40
 */
@Getter
@Setter
@Schema(description = "系统菜单树VO")
public class SysMenuTreeVO extends TreeNodeVO<SysMenu> {
    /**
     * 父级菜单id
     */
    @Schema(description = "父级菜单id")
    private String parentId;

    /**
     * 菜单类型(0:目录|1:菜单)
     */
    @Schema(description = "菜单类型(0:目录|1:菜单")
    private Integer menuType;

    /**
     * 菜单编码
     */
    @Schema(description = "菜单编码")
    private String menuCode;

    /**
     * 菜单标题
     */
    @Schema(description = "菜单标题")
    private String menuTitle;

    /**
     * 菜单图标
     */
    @Schema(description = "菜单图标")
    private String menuIcon;

    /**
     * 菜单路径(菜单url地址)
     */
    @Schema(description = "菜单路径(菜单url地址)")
    private String menuPath;

    /**
     * 页面组件(页面目录地址)
     */
    @Schema(description = "页面组件(页面目录地址)")
    private String redirect;

    /**
     * 排序
     */
    @Schema(description = "页面组件(排序)")
    private Integer orderNum;

    /**
     * 构建菜单树
     *
     * @param sysMenu 菜单基本信息
     */
    public SysMenuTreeVO(SysMenu sysMenu) {
        this.id = sysMenu.getId();
        this.parentId = sysMenu.getParentId();
        this.menuTitle = sysMenu.getMenuTitle();
        this.label = sysMenu.getMenuTitle();
        this.menuType = sysMenu.getMenuType();
        this.menuPath = sysMenu.getMenuPath();
        this.orderNum = sysMenu.getOrderNum();
        this.menuCode = sysMenu.getMenuCode();
        this.menuIcon = sysMenu.getMenuIcon();
        this.redirect = sysMenu.getRedirect();
    }
}