package com.cs.cslc.admin.rest;

import com.cs.cslc.admin.entity.BaseMessage;
import com.cs.cslc.admin.service.MessageServiceImpl;
import com.cs.cslc.common.rest.BaseController;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Message 系统消息.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2023-10-24
 */
@RestController
@RequestMapping("/admin/base/message")
@Tag(name = "系统消息服务")
public class BaseMessageController extends BaseController<MessageServiceImpl, BaseMessage> {

}