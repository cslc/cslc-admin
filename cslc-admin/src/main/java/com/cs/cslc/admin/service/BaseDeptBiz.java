/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cs.cslc.admin.entity.BaseDept;
import com.cs.cslc.admin.entity.BaseDeptUser;
import com.cs.cslc.admin.mapper.BaseDeptMapper;
import com.cs.cslc.admin.vo.BaseDeptTreeVO;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import com.cs.cslc.common.constant.CommonConstant;
import com.cs.cslc.common.constant.InitialCapacityConstant;
import com.cs.cslc.common.exception.BusinessException;
import com.cs.cslc.common.pojo.ParamQuery;
import com.cs.cslc.common.util.BeanUtil;
import com.cs.cslc.common.util.TreeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * BaseDept 基础部门信息表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-12-06 09:14
 */
@Service
public class BaseDeptBiz extends BaseBusinessBiz<BaseDeptMapper, BaseDept> {

    @Autowired
    private BaseDeptUserBiz deptUserBiz;

    @Override
    public boolean insertModel(BaseDept entity) {
        Long count = this.getDeptByDeptCode(entity.getDeptCode());
        if (count > 0) {
            throw new BusinessException("部门编码已存在！");
        }

        if (StringUtils.isBlank(entity.getParentId())) {
            entity.setParentId(CommonConstant.ROOT);
        }
        return super.insertModel(entity);
    }

    @Override
    public boolean removeById(Serializable id) {
        if (null == id) {
            throw new BusinessException("请选择待删除的部门！");
        }
        BaseDept baseDept = this.getById(id);
        String deptCode = baseDept.getDeptCode();

        String deptId = String.valueOf(id);
        Long count = this.getSonCountByParentId(deptId);
        if (count > 0) {
            throw new BusinessException("请先删除部门的子部门！");
        }
        // 判断是否存在用户和部门的关系是否删除，有关系不允许删除
        Long userCount = deptUserBiz.getDeptSizeByDeptCode(deptCode);
        if (userCount > 0) {
            throw new BusinessException("该部门存在" + userCount + "个用户，请先移除部门中的用户！");
        }

        // 更新删除db的code，防止code无法设置新的
        baseDept.setDeptCode(id + deptCode);
        this.updateById(baseDept);

        // 逻辑删除
        return super.removeById(deptId);
    }

    /**
     * 通过部门编码统计历史部门个数
     *
     * @param deptCode 部门编码
     * @return 历史个数
     */
    public Long getDeptByDeptCode(String deptCode) {
        LambdaQueryWrapper<BaseDept> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(BaseDept::getDeptCode, deptCode);
        return this.baseMapper.selectCount(queryWrapper);
    }

    /**
     * 通过部门编码统计历史部门个数
     *
     * @param parentId 父级主键id
     * @return 儿子个数
     */
    public Long getSonCountByParentId(String parentId) {
        LambdaQueryWrapper<BaseDept> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(BaseDept::getParentId, parentId);
        return this.baseMapper.selectCount(queryWrapper);
    }

    /**
     * 获取部门树
     *
     * @return 部门树
     */
    public List<BaseDeptTreeVO> getBaseDeptTree(String deptName) {
        String[] fields = {"id", "parent_id", "dept_code", "dept_name", "update_time", "create_time"};
        // 获取全部数据
        String orderByClause = "create_time";

        // TODO 部门树形过滤
        ParamQuery query = null;
        if (StringUtils.isNotBlank(deptName)) {
            Map<String, Object> paramQuery = new HashMap<>();
            paramQuery.put("deptName", deptName);
            query = new ParamQuery(paramQuery);
        }

        List<BaseDept> deptList = this.selectFieldListAllAsc(query, fields, orderByClause);
        // 缓存树型结构数据
        List<BaseDeptTreeVO> tree = new ArrayList<>(InitialCapacityConstant.INITIAL_128_NUMBER);
        // 构造数据
        if (BeanUtil.isNotEmpty(deptList)) {
            for (BaseDept baseDept : deptList) {
                // 根节点展开
                tree.add(new BaseDeptTreeVO(baseDept));
            }
        }
        List<BaseDeptTreeVO> result = TreeUtil.buildByRecursive(tree, CommonConstant.ROOT);
        return BeanUtil.isEmpty(result) ? new ArrayList<>() : result;
    }

    /**
     * 添加部门用户关系
     *
     * @param deptCode 部门编码
     * @param userIds  用户集合
     */
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void addDeptUser(String deptCode, List<String> userIds) {
        if (BeanUtil.isNotEmpty(userIds)) {
            for (String userId : userIds) {
                BaseDeptUser deptUser = new BaseDeptUser();
                deptUser.setDeptCode(deptCode);
                deptUser.setUserId(userId);
                deptUserBiz.insertModel(deptUser);
            }
        }
    }

    /**
     * 通过部门编码和用户删除
     *
     * @param deptCode 部门编码
     * @param userId   用户主键
     */
    public void delDeptUserByUserId(String deptCode, String userId) {
        if (StringUtils.isNotBlank(deptCode) && StringUtils.isNotBlank(userId)) {
            BaseDeptUser deptUser = new BaseDeptUser();
            deptUser.setUserId(userId);
            deptUser.setDeptCode(deptCode);
            deptUserBiz.delDeptUser(deptUser);
        }
    }
}