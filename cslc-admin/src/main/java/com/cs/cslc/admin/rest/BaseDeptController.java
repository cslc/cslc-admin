/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.rest;

import com.cs.cslc.admin.entity.BaseDept;
import com.cs.cslc.admin.service.BaseDeptBiz;
import com.cs.cslc.admin.vo.BaseDeptTreeVO;
import com.cs.cslc.admin.vo.DeptUserVO;
import com.cs.cslc.common.constant.HttpStatusConstant;
import com.cs.cslc.common.msg.ObjectResult;
import com.cs.cslc.common.rest.BaseController;
import com.cs.cslc.common.util.BeanUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * BaseDept 基础部门信息表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-12-06 09:14
 */
@RestController
@RequestMapping("/admin/baseDepts")
@Tag(name = "基础部门信息表")
public class BaseDeptController extends BaseController<BaseDeptBiz, BaseDept> {

    /**
     * 获取部门树
     *
     * @return 部门树
     */
    @GetMapping("/tree")
    @Operation(summary = "获取部门树")
    public ObjectResult<List<BaseDeptTreeVO>> getBaseDeptTree(@RequestParam(value = "deptName", required = false)
                                                              @Parameter(name = "部门名称") String deptName) {
        ObjectResult<List<BaseDeptTreeVO>> result = new ObjectResult<>();
        result.setData(this.baseBiz.getBaseDeptTree(deptName));
        return result;
    }

    /**
     * 添加部门用户关系
     *
     * @param deptCode 部门编码
     * @param deptUser 用户集合
     */
    @PutMapping("/deptUsers/{deptCode}")
    @Operation(summary = "添加部门用户关系")
    public ObjectResult<Void> addDeptUser(@PathVariable("deptCode") @Parameter(name = "部门编码") String deptCode,
                                          @RequestBody DeptUserVO deptUser) {
        ObjectResult<Void> result = new ObjectResult<>();
        List<String> userIds = deptUser.getUserIds();
        if (BeanUtil.isEmpty(userIds)) {
            result.setMessage("请选择待分配的用户！");
            result.setStatus(HttpStatusConstant.FAIL);
            return result;
        }
        this.baseBiz.addDeptUser(deptCode, userIds);
        return result;
    }

    /**
     * 删除部门的用户关系
     *
     * @param deptCode 部门编码
     * @param userId   用户编码
     */
    @DeleteMapping("/deptUsers/{deptCode}/userId/{userId}")
    @Operation(summary = "删除部门的用户关系")
    public ObjectResult<Void> delDeptUserByUserId(@PathVariable("deptCode") @Parameter(name = "部门编码") String deptCode,
                                                  @PathVariable("userId") @Parameter(name = "用户主键") String userId) {
        ObjectResult<Void> result = new ObjectResult<>();
        this.baseBiz.delDeptUserByUserId(deptCode, userId);
        return result;
    }
}