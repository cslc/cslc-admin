/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.service;

import com.cs.cslc.admin.entity.SysTenant;
import com.cs.cslc.admin.mapper.SysTenantMapper;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import com.cs.cslc.common.util.BeanUtil;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * SysTenant 基础租户表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-02 10:01:49
 */
@Service
public class SysTenantBiz extends BaseBusinessBiz<SysTenantMapper, SysTenant> {
    /**
     * 通过实体，新增单条数据
     * <p>
     * 保存一个实体，null的属性不会保存，会使用数据库默认值
     * </p>
     *
     * @param entity 实体
     */
    @Override
    public boolean insertModel(SysTenant entity) {
        BeanUtil.beanAttributeValueTrim(entity);
        entity.setTenantCode(this.findTenantCode());
        return super.save(entity);
    }

    /**
     * 时间戳+3位随机数
     *
     * @return 租户编码
     */
    private String findTenantCode() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String tenantCode = formatter.format(new Date());
        tenantCode += new SecureRandom().nextInt(1000);
        return tenantCode;
    }
}