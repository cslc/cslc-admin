/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cs.cslc.admin.entity.SysDictType;
import com.cs.cslc.admin.mapper.SysDictTypeMapper;
import com.cs.cslc.admin.vo.SysDictTypeTreeVO;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import com.cs.cslc.common.constant.CommonConstant;
import com.cs.cslc.common.exception.BusinessException;
import com.cs.cslc.common.pojo.ParamQuery;
import com.cs.cslc.common.util.BeanUtil;
import com.cs.cslc.common.util.TreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * SysDictType 数据字典类型.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-07 21:41
 */
@Service
public class SysDictTypeBiz extends BaseBusinessBiz<SysDictTypeMapper, SysDictType> {
    @Autowired
    private SysDictValueBiz sysDictValueBiz;

    @Override
    public boolean insertModel(SysDictType entity) {
        Long count = this.getByDictTypeCode(entity.getDictTypeCode());
        if (count > 0) {
            throw new BusinessException("字典类型编码已存在！");
        }
        return super.insertModel(entity);
    }

    @Override
    public boolean removeById(Serializable id) {
        SysDictType dictType = super.getById(id);
        if (dictType == null) {
            throw new BusinessException("未找到删除的字典类型！");
        }

        long size = sysDictValueBiz.getSizeByTypeId(String.valueOf(id));
        if (size > 0) {
            throw new BusinessException("存在" + size + "个值，请先删除字典值！");
        }
        // 更新编码
        dictType.setDictTypeCode(id + dictType.getDictTypeCode());
        super.updateById(dictType);

        // 逻辑删除
        return super.removeById(dictType);
    }

    @Override
    public List<SysDictType> selectListAll(ParamQuery query) {
        List<SysDictType> dataList = super.selectFieldListAllDesc(query,
                new String[]{"id", "dict_type_name", "dict_type_code", "parent_id"}, null);
        return BeanUtil.isNotEmpty(dataList) ? dataList : new ArrayList<>();
    }

    /**
     * 获取数据类型树结构
     *
     * @return 匹配数据
     */
    public List<SysDictTypeTreeVO> getDictTypeTree() {
        // 获取全部数据
        List<SysDictType> dictTypeList = this.selectListAll(null);
        // 缓存树型结构数据
        List<SysDictTypeTreeVO> tree = new ArrayList<>();
        // 构造数据
        if (BeanUtil.isNotEmpty(dictTypeList)) {
            for (SysDictType dictType : dictTypeList) {
                if (CommonConstant.ROOT.equals(dictType.getParentId())) {
                    // 根节点展开
                    tree.add(new SysDictTypeTreeVO(dictType.getId(), dictType.getParentId(), dictType.getDictTypeName(), dictType.getDictTypeCode()));
                } else {
                    // 二级节点不展开
                    tree.add(new SysDictTypeTreeVO(dictType.getId(), dictType.getParentId(), dictType.getDictTypeName(), dictType.getDictTypeCode(), false));
                }
            }
        }
        List<SysDictTypeTreeVO> result = TreeUtil.buildByRecursive(tree, CommonConstant.ROOT);
        return BeanUtil.isEmpty(result) ? new ArrayList<>() : result;
    }

    /**
     * 根据code条件进行查询总数
     *
     * @param dictTypeCode 编码
     * @return 匹配个数
     */
    public Long getByDictTypeCode(String dictTypeCode) {
        LambdaQueryWrapper<SysDictType> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysDictType::getDictTypeCode, dictTypeCode);
        return this.baseMapper.selectCount(queryWrapper);
    }

    /**
     * 通过编码code获取匹配集合
     *
     * @param dictCode 编码
     * @return 匹配数据
     */
    public List<SysDictType> findByDictCode(String dictCode) {
        LambdaQueryWrapper<SysDictType> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(SysDictType::getDictTypeCode, SysDictType::getDictTypeName);
        queryWrapper.like(SysDictType::getDictTypeCode, dictCode);
        queryWrapper.orderByAsc(SysDictType::getOrderNum);
        return this.baseMapper.selectList(queryWrapper);
    }

    /**
     * 通过parentId获取子typeCode匹配集合
     *
     * @param parentId 编码
     * @return 匹配数据
     */
    public List<SysDictType> findSonTypeByParentId(String parentId) {
        LambdaQueryWrapper<SysDictType> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(SysDictType::getDictTypeCode, SysDictType::getDictTypeName, SysDictType::getDescription);
        queryWrapper.like(SysDictType::getParentId, parentId);
        queryWrapper.orderByAsc(SysDictType::getOrderNum);
        return this.baseMapper.selectList(queryWrapper);
    }
}