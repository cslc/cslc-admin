/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * SysTenant 基础租户表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-02 10:01:49
 */
@Getter
@Setter
@TableName("sys_tenant")
@Schema(description = "基础租户")
public class SysTenant implements Serializable {

    private static final long serialVersionUID = -657329433646543208L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_UUID)
    @Schema(description = "主键")
    private String id;

    /**
     * 租户编码
     */
    @Schema(description = "租户编码")
    @TableField("tenant_code")
    private String tenantCode;

    /**
     * 租户名称
     */
    @Schema(description = "租户名称")
    @TableField("tenant_name")
    private String tenantName;

    /**
     * 租户说明
     */
    @Schema(description = "租户说明")
    @TableField("description")
    private String description;

    /**
     * 是否删除（1是|0否）
     */
    @TableLogic
    @Schema(description = "是否删除")
    @TableField("is_deleted")
    private Integer isDeleted;

    /**
     * 是否超级租户（1是|0否）
     */
    @Schema(description = "是否超级租户")
    @TableField("is_super_tenant")
    private Integer isSuperTenant;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建日期")
    private Date createTime;
    /**
     * 创建用户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建用户Id")
    private String createUserId;
    /**
     * 创建用户姓名
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建用户姓名")
    private String createUserName;
    /**
     * 最后更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新日期")
    private Date updateTime;
    /**
     * 最后更新用户Id
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新用户Id")
    private String updateUserId;
    /**
     * 最后更新用户姓名
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新用户姓名")
    private String updateUserName;
    /**
     * 租户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "租户Id")
    private String tenantId;

}