package com.cs.cslc.admin.service;

import com.cs.cslc.admin.entity.BaseMessage;
import com.cs.cslc.admin.mapper.BaseMessageMapper;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import org.springframework.stereotype.Service;

/**
 * Message 系统消息.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2023-10-24
 */
@Service
public class MessageServiceImpl extends BaseBusinessBiz<BaseMessageMapper, BaseMessage> {
}