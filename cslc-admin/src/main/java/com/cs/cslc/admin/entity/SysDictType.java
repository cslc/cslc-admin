/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * SysDictType 数据字典类型.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-07 21:41
 */
@Getter
@Setter
@TableName("sys_dict_type")
@Schema(description = "数据字典类型")
public class SysDictType implements Serializable {

    private static final long serialVersionUID = 8751266573670656242L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_UUID)
    @Schema(description = "主键")
    private String id;

    /**
     * 类型编码
     */
    @Schema(description = "类型编码")
    @Size(min = 1, max = 128)
    @TableField("dict_type_code")
    private String dictTypeCode;

    /**
     * 类型名称
     */
    @Schema(description = "类型名称")
    @Size(min = 1, max = 256)
    @TableField("dict_type_name")
    private String dictTypeName;

    /**
     * 父id
     */
    @Schema(description = "父id")
    @TableField("parent_id")
    private String parentId;

    /**
     * 排序
     */
    @Schema(description = "排序")
    @TableField("order_num")
    private Integer orderNum;

    /**
     * 角色描述
     */
    @Schema(description = "角色描述")
    @TableField("description")
    private String description;

    /**
     * 是否删除(1:删除|0:未删除)
     */
    @TableLogic
    @Schema(description = "是否删除")
    @TableField("is_deleted")
    private Integer isDeleted;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建日期")
    private Date createTime;
    /**
     * 创建用户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建用户Id")
    private String createUserId;
    /**
     * 创建用户姓名
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建用户姓名")
    private String createUserName;
    /**
     * 最后更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新日期")
    private Date updateTime;
    /**
     * 最后更新用户Id
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新用户Id")
    private String updateUserId;
    /**
     * 最后更新用户姓名
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新用户姓名")
    private String updateUserName;
    /**
     * 租户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "租户Id")
    private String tenantId;
}