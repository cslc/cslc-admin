/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cs.cslc.admin.entity.SysDictValue;
import com.cs.cslc.admin.mapper.SysDictValueMapper;
import com.cs.cslc.common.biz.BaseBusinessBiz;
import com.cs.cslc.common.constant.InitialCapacityConstant;
import com.cs.cslc.common.exception.BusinessException;
import com.cs.cslc.common.msg.TableResult;
import com.cs.cslc.common.pojo.ParamQuery;
import com.cs.cslc.common.util.BeanUtil;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * SysDictValue 数据字典值.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-10-07 21:41
 */
@Service
public class SysDictValueBiz extends BaseBusinessBiz<SysDictValueMapper, SysDictValue> {

    @Override
    public boolean insertModel(SysDictValue entity) {
        Long count = this.getByDictTypeCode(entity.getDictCode());
        if (count > 0) {
            throw new BusinessException("字典编码已存在！");
        }
        return super.insertModel(entity);
    }


    @Override
    public boolean removeById(Serializable id) {
        SysDictValue dictValue = baseMapper.selectById(id);
        if (dictValue == null) {
            throw new BusinessException("未找到删除的字典值！");
        }
        // 更新编码
        dictValue.setDictCode(id + dictValue.getDictCode());
        super.updateById(dictValue);

        // 逻辑删除
        return super.removeById(dictValue);
    }

    @Override
    public List<SysDictValue> selectListAll(ParamQuery query) {
        List<SysDictValue> dataList = super.selectFieldListAllDesc(query,
                new String[]{"id", "dict_title", "dict_code", "type_id"}, null);
        return BeanUtil.isNotEmpty(dataList) ? dataList : new ArrayList<>();
    }

    @Override
    public TableResult<SysDictValue> selectTableByParamQuery(ParamQuery query) {
        LambdaQueryWrapper<SysDictValue> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(SysDictValue::getId, SysDictValue::getDictCode, SysDictValue::getDictTitle,
                SysDictValue::getDescription, SysDictValue::getTypeId, SysDictValue::getOrderNum, SysDictValue::getUpdateTime);
        queryWrapper.like(SysDictValue::getTypeId, query.get("typeId"));
        queryWrapper.orderByAsc(SysDictValue::getOrderNum);
        List<SysDictValue> list = this.baseMapper.selectList(queryWrapper);
        return new TableResult<>(list.size(), list);
    }

    /**
     * 根据code条件进行查询总数
     *
     * @param dictCode 编码
     * @return 匹配个数
     */
    public Long getByDictTypeCode(String dictCode) {
        LambdaQueryWrapper<SysDictValue> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysDictValue::getDictCode, dictCode);
        return this.baseMapper.selectCount(queryWrapper);
    }

    /**
     * 通过编码code获取匹配集合
     *
     * @param dictCode 编码
     * @return 匹配数据
     */
    public List<SysDictValue> findByDictCode(String dictCode) {
        LambdaQueryWrapper<SysDictValue> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(SysDictValue::getDictCode, SysDictValue::getDictTitle,
                SysDictValue::getDescription);
        queryWrapper.like(SysDictValue::getDictCode, dictCode);
        queryWrapper.orderByAsc(SysDictValue::getOrderNum);
        return this.baseMapper.selectList(queryWrapper);
    }

    /**
     * 通过字典类型编码值获取子集
     *
     * @param dictTypeCode 字典类型编码
     * @return 匹配的子集[DictCode, DictTitle]
     */
    public Map<String, String> getMapDictValue(String dictTypeCode) {
        Map<String, String> result = new HashMap<>(InitialCapacityConstant.INITIAL_64_NUMBER);
        LambdaQueryWrapper<SysDictValue> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(SysDictValue::getDictCode, dictTypeCode);

        List<SysDictValue> dictValueList = this.baseMapper.selectList(queryWrapper);
        if (BeanUtil.isNotEmpty(dictValueList)) {
            result.putAll(dictValueList.stream()
                    .collect(Collectors.toMap(SysDictValue::getDictCode, SysDictValue::getDictTitle, (key1, key2) -> key1)));
        }
        return result;
    }

    /**
     * 根据字典类型条件进行查询总数
     *
     * @param typeId 字典类型
     * @return 匹配个数
     */
    public Long getSizeByTypeId(String typeId) {
        LambdaQueryWrapper<SysDictValue> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysDictValue::getTypeId, typeId);
        return this.baseMapper.selectCount(queryWrapper);
    }
}