/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.rest;

import com.cs.cslc.admin.dto.UserInfoDTO;
import com.cs.cslc.admin.entity.BaseUser;
import com.cs.cslc.admin.service.BaseUserBiz;
import com.cs.cslc.admin.vo.BaseUserVO;
import com.cs.cslc.common.constant.HttpStatusConstant;
import com.cs.cslc.common.constant.MessageConstant;
import com.cs.cslc.common.msg.ObjectResult;
import com.cs.cslc.common.msg.TableResult;
import com.cs.cslc.common.rest.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * BaseUserController 基础用户表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2019-09-30 15:23:58
 */
@Slf4j
@RestController
@RequestMapping("/admin/baseUsers")
@Tag(name = "基础用户模块")
public class BaseUserController extends BaseController<BaseUserBiz, BaseUser> {

    @GetMapping("/info")
    @Operation(summary = "获取用户信息：用户基本信息、菜单权限列表")
    public ObjectResult<UserInfoDTO> getUserInfo() {
        ObjectResult<UserInfoDTO> res = new ObjectResult<>();
        res.setData(this.baseBiz.getUserInfo());
        return res;
    }

    @GetMapping("/permission")
    @Operation(summary = "获取用户权限：菜单权限列表")
    public ObjectResult<UserInfoDTO> getUserPermission() {
        ObjectResult<UserInfoDTO> res = new ObjectResult<>();
        res.setData(this.baseBiz.getUserPermission());
        return res;
    }

    @PutMapping("/update")
    @Operation(summary = "更新用户基本信息")
    public ObjectResult<BaseUser> updateUser(@RequestBody @Parameter(name = "基本信息") BaseUser entity) {
        ObjectResult<BaseUser> result = new ObjectResult<>();
        String userId = entity.getId();
        if (StringUtils.isBlank(userId)) {
            result.setStatus(HttpStatusConstant.FAIL);
            result.setMessage("请求参数异常");
            return result;
        }

        try {
            baseBiz.updateById(entity);
            result.setData(entity);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setStatus(HttpStatusConstant.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 修改密码
     *
     * @param baseUserVO 用户信息
     */
    @PutMapping("/password")
    @Operation(summary = "修改密码")
    public ObjectResult<BaseUser> changePassword(@RequestBody BaseUserVO baseUserVO) {
        String password = baseUserVO.getPassword();
        String newPassword = baseUserVO.getNewPassword();

        // 验证密码是否为空
        if (StringUtils.isBlank(password) || StringUtils.isBlank(newPassword)) {
            ObjectResult<BaseUser> result = new ObjectResult();
            result.setStatus(HttpStatusConstant.FAIL);
            result.setMessage(MessageConstant.MVC_VALIDATE_MSG);
            return result;
        }

        return this.baseBiz.changePassword(password, newPassword);
    }

    /**
     * 通过部门编码获取用户列表
     *
     * @param deptCode 部门编码
     * @return 指定部门的用户列表
     */
    @GetMapping("/deptCode/{deptCode}")
    @Operation(summary = "指定部门的用户列表")
    public TableResult<BaseUser> getDeptUsersByDeptCode(
            @PathVariable("deptCode") @Parameter(name = "部门编码") String deptCode,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit) {
        return this.baseBiz.getDeptUsersByDeptCode(deptCode, page, limit);
    }

    /**
     * 排除指定部门的用户列表
     *
     * @param excludeDeptCode 排除指定部门
     * @param page            页码
     * @param limit           页容量
     * @return 排除指定部门的用户列表
     */
    @GetMapping("/exclude/{excludeDeptCode}")
    @Operation(summary = "排除指定部门的用户列表")
    public TableResult<BaseUser> getUsersExcludeDept(
            @PathVariable("excludeDeptCode") @Parameter(name = "排除指定部门") String excludeDeptCode,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit) {
        return this.baseBiz.getUsersExcludeDept(excludeDeptCode, page, limit);
    }

    /**
     * 获取用户角色编码列表
     *
     * @return 获取用户角色编码列表
     */
    @GetMapping("/{userId}/role")
    @Operation(summary = "获取用户角色编码列表")
    public ObjectResult<List<String>> getUserRolesByUserId(
            @PathVariable("userId") String userId) {
        ObjectResult<List<String>> result = new ObjectResult<>();
        result.setData(this.baseBiz.getUserRolesByUserId(userId));
        return result;
    }

    /**
     * 更新用户的角色
     *
     * @return 更新用户的角色
     */
    @PutMapping("/{userId}/role")
    @Operation(summary = "更新用户角色编码列表")
    public ObjectResult<Void> setUserRole(
            @PathVariable("userId") String userId,
            @RequestBody BaseUserVO user) {
        ObjectResult<Void> result = new ObjectResult<>();
        this.baseBiz.setUserRole(userId, user.getRoleCodes());
        return result;
    }
}