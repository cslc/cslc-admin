/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.admin.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.cs.cslc.common.validate.AddField;
import com.cs.cslc.common.validate.UpdateField;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * BaseUser 基础用户表.
 *
 * @author cs
 * @version 1.0
 * @LocalDateTime 2019-09-30 15:23:58
 */
@Getter
@Setter
@TableName("base_user")
@Schema(description = "基础用户")
public class BaseUser implements Serializable {

    private static final long serialVersionUID = -7543288892230630859L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_UUID)
    @Schema(description = "主键")
    @NotBlank(groups = UpdateField.class)
    @Size(min = 1, max = 64, groups = UpdateField.class)
    private String id;

    /**
     * 用户账户
     */
    @Schema(description = "用户账户")
    @NotBlank(groups = {AddField.class, UpdateField.class})
    @Size(min = 3, max = 64, groups = {AddField.class, UpdateField.class})
    @TableField("account")
    private String account;

    /**
     * 用户账户
     */
    @Schema(description = "wx_unionid")
    @TableField("wx_unionid")
    private String wxUnionid;

    /**
     * 用户密码
     */
    @Schema(description = "用户密码")
    @NotBlank(groups = AddField.class)
    @Size(max = 256, groups = AddField.class)
    @TableField("password")
    private String password;

    /**
     * 用户姓名
     */
    @Schema(description = "用户姓名")
    @TableField("user_name")
    private String userName;

    /**
     * 用户性别
     */
    @Schema(description = "用户性别")
    @TableField("user_sex")
    private Integer userSex;

    /**
     * 用户头像
     */
    @Schema(description = "用户头像")
    @TableField("portrait")
    private String portrait;

    /**
     * 用户生日
     */
    @Schema(description = "用户生日")
    @TableField("birthday")
    private LocalDateTime birthday;

    /**
     * 用户地址
     */
    @Schema(description = "用户地址")
    @TableField("address")
    private String address;

    /**
     * 用户手机号
     */
    @Schema(description = "用户手机号")
    @TableField("mobile_phone")
    private String mobilePhone;

    /**
     * 用户邮箱
     */
    @Schema(description = "用户邮箱")
    @TableField("user_email")
    private String userEmail;

    /**
     * 用户说明
     */
    @Schema(description = "用户说明")
    @TableField("description")
    private String description;

    /**
     * 是否删除（1是|0否）
     */
    @Schema(description = "是否删除")
    @TableField("is_deleted")
    private Integer isDeleted;

    /**
     * 是否禁用（1是|0否）
     */
    @TableLogic
    @Schema(description = "是否禁用")
    @TableField("is_disabled")
    private Integer isDisabled;

    /**
     * 是否超级管理员（1是|0否）
     */
    @Schema(description = "是否超级管理员")
    @TableField("is_super_admin")
    private Integer isSuperAdmin;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建日期")
    private Date createTime;
    /**
     * 创建用户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建用户Id")
    private String createUserId;
    /**
     * 创建用户姓名
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "创建用户姓名")
    private String createUserName;
    /**
     * 最后更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新日期")
    private Date updateTime;
    /**
     * 最后更新用户Id
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新用户Id")
    private String updateUserId;
    /**
     * 最后更新用户姓名
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Schema(description = "最后更新用户姓名")
    private String updateUserName;
    /**
     * 租户Id
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(description = "租户Id")
    private String tenantId;

}