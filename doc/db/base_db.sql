-- --------------
-- 创建数据库
-- --------------
CREATE DATABASE base_db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
Use base_db;


SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_dept
-- ----------------------------
DROP TABLE IF EXISTS `base_dept`;
CREATE TABLE `base_dept`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `parent_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'root' COMMENT '父级部门id',
  `dept_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  `dept_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门编码',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门说明',
  `is_deleted` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除(1:删除|0:未删除)',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime(0) NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dept_code`(`dept_code`) USING BTREE COMMENT '部门编码'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '基础部门信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_dept
-- ----------------------------
INSERT INTO `base_dept` VALUES ('0aa8c2a89da143fbbcb5ffbec1c0fc2a', 'root', '默认部门', 'default', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');

-- ----------------------------
-- Table structure for base_dept_user
-- ----------------------------
DROP TABLE IF EXISTS `base_dept_user`;
CREATE TABLE `base_dept_user`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `dept_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门编码',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '基础角色部门关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_file
-- ----------------------------
DROP TABLE IF EXISTS `base_file`;
CREATE TABLE `base_file`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sign` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件唯一标志',
  `fileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件名',
  `suffix` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '扩展名',
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件大小（单位:KB）',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_message
-- ----------------------------
DROP TABLE IF EXISTS `base_message`;
CREATE TABLE `base_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(36) NOT NULL COMMENT '消息标题',
  `type` varchar(100) NOT NULL COMMENT '消息类型',
  `content` varchar(1024) NOT NULL COMMENT '消息内容',
  `is_deleted` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除(1:删除|0:未删除)',
  `create_time` datetime NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) NOT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统消息表';

INSERT INTO `base_message` VALUES (1, '1.3.0正式发布', 'SYSTEM_UPGRADE', '<p>1.3.0正式发布</p>', 0, '2024-10-01 11:14:36', 'admin', 'admin', '2024-10-01 11:14:36', 'admin', 'admin', 'admin');

-- ----------------------------
-- Table structure for base_message_read
-- ----------------------------
DROP TABLE IF EXISTS `base_message_read`;
CREATE TABLE `base_message_read`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `is_sign` int(0) NOT NULL COMMENT '已读(1:已读|0:未读)',
  `is_deleted` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除(1:删除|0:未删除)',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime(0) NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息读取记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `account` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账户',
  `wx_unionid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户在微信开放平台的唯一标识符',
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `user_sex` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户性别（0男|1女）',
  `portrait` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '用户生日',
  `address` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户地址',
  `mobile_phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户手机号',
  `user_email` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户说明',
  `is_deleted` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除(1:删除|0:未删除)',
  `is_disabled` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否禁用（1是|0否）',
  `is_super_admin` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否超级管理员（1是|0否）',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime(0) NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account`(`account`) USING BTREE COMMENT '用户账户'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '基础用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES ('admin', 'admin', NULL, '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '超级管理员', 0, NULL, NULL, NULL, '12312341324', 'cs4380@163.com', NULL, 0, 0, 1, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');

-- ----------------------------
-- Table structure for base_user_role
-- ----------------------------
DROP TABLE IF EXISTS `base_user_role`;
CREATE TABLE `base_user_role`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户Id',
  `role_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '基础用户角色关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user_role
-- ----------------------------
INSERT INTO `base_user_role` VALUES (1, '2', 'default', 'admin');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `dict_type_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型编码',
  `dict_type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型名称',
  `parent_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父id',
  `order_num` int(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型说明',
  `is_deleted` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除(1:删除|0:未删除)',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime(0) NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_type_code`(`dict_type_code`) USING BTREE COMMENT '类型编码'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', 'biz', '基础数据', 'root', 1, NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_dict_type` VALUES ('2', 'root_biz_sex', '性别', '1', 1, NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');

-- ----------------------------
-- Table structure for sys_dict_value
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_value`;
CREATE TABLE `sys_dict_value`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `type_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据字典类型id',
  `dict_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码',
  `dict_title` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典标题',
  `order_num` int(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典值说明',
  `is_deleted` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除(1:删除|0:未删除)',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime(0) NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_code`(`dict_code`) USING BTREE COMMENT '字典编码'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典值' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_value
-- ----------------------------
INSERT INTO `sys_dict_value` VALUES ('62e80bb0c9c9485bbd38778bcb08069a', '2', 'root_biz_sex_nv', '女', 1, '女', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_dict_value` VALUES ('e03e27f61b9d463fab0dd01a171a9668', '2', 'root_biz_sex_nan', '男', 1, '男', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'root' COMMENT '主键',
  `parent_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'root' COMMENT '父级菜单id',
  `menu_type` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '菜单类型(0:目录|1:菜单)',
  `menu_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单编码',
  `menu_title` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单标题',
  `menu_icon` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `menu_path` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '/' COMMENT '菜单路径（路径别名）',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单组件地址(菜单url地址)',
  `redirect` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重定向地址',
  `order_num` int(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
  `hidden` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否隐藏菜单（0不隐藏,1隐藏）',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单描述',
  `is_deleted` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除(1:删除|0:未删除)',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime(0) NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `menu_code`(`menu_code`) USING BTREE COMMENT '按钮编码'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统基础菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('01549dbf3c6542b99c7559f5d2883bdc', '80e863628d5f453981dcbb853a3ef953', 1, 'sysMenu', '菜单管理', 'menu', 'sysMenu', '/system/sys/menu/index', NULL, 3, 0, '菜单管理', 0, '2023-10-24 13:57:33', 'admin', 'admin', '2024-09-10 17:09:57', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('1422833c6ab044f5b89261bace41ad45', '21bfe17560284601a7c1b88d6b01f607', 1, 'dashboard', '仪表板', 'dashboard', 'dashboard', '/center/dashboard/index', NULL, 1, 0, '个人主页', 0, '2023-10-24 12:04:31', 'admin', 'admin', '2024-09-10 17:07:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('21bfe17560284601a7c1b88d6b01f607', 'root', 0, 'center', '个人中心', 'people', '/center', NULL, '/center/personal', 1, 0, '个人中心', 0, '2023-10-24 01:05:47', 'admin', 'admin', '2024-09-12 16:57:52', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('224767b51e954e3299a49a92913c3128', '80e863628d5f4539814cbb853a3ef953', 1, 'deptUser', '部门用户', 'dept-user', 'deptUser', '/system/base/deptUser/index', NULL, 3, 0, NULL, 0, '2023-10-24 13:16:20', 'admin', 'admin', '2024-09-10 17:25:36', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('49992d462a004f6d95d53a6e95fa6071', '80e863628d5f453981dcbb853a3ef953', 1, 'sysRole', '角色管理', 'role', 'sysRole', '/system/sys/role/index', NULL, 4, 0, '角色管理', 0, '2023-10-24 13:58:05', 'admin', 'admin', '2024-09-10 17:12:50', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('49f5a59dfe5a4f22832052e7ad0b9f48', '80e863628d5f453981dcbb853a3ef953', 1, 'swagger', '接口文档', 'swagger', 'swagger', '/system/sys/swagger/index', NULL, 6, 0, '接口文档', 0, '2023-10-24 13:58:28', 'admin', 'admin', '2024-09-10 17:20:42', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('5ed6df281aa2492cb9f026bc393db58d', '80e863628d5f4539814cbb853a3ef953', 1, 'baseUser', '用户管理', 'peoples', 'baseUser', '/system/base/user/index', NULL, 1, 0, '角色管理', 0, '2023-10-24 12:13:14', 'admin', 'admin', '2024-09-10 17:22:23', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('80e863628d5f4539814cbb853a3ef953', 'root', 0, 'base', '系统管理', 'example', '/base', NULL, '/system/sysDict', 6, 0, '系统管理', 0, '2023-10-24 13:56:24', 'admin', 'admin', '2023-10-24 01:49:43', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('80e863628d5f453981dcbb853a3ef953', 'root', 0, 'system', '系统配置管理', 'settings', '/system', NULL, '/system/sysDict', 7, 0, '系统配置中心', 0, '2023-10-24 13:56:24', 'admin', 'admin', '2024-09-10 17:22:02', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('94591e71452e06b4379239a91cc07587', 'root', 0, 'bizManage', '业务管理', 'component', '/bizManage', NULL, NULL, 2, 0, '业务管理', 0, '2023-10-25 23:17:45', 'admin', 'admin', '2023-10-25 23:17:45', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('a1a70a12612d4330b01268ba4f63e068', '21bfe17560284601a7c1b88d6b01f607', 1, 'centerPersonal', '个人信息', 'people', 'personal', '/center/personal/index', '', 2, 0, '', 0, '2023-10-24 13:35:38', 'admin', 'admin', '2024-09-10 17:06:45', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('a789477a626041bd86aecbd2ae995d16', '80e863628d5f453981dcbb853a3ef953', 1, 'sysDict', '数据字典', 'dict', 'sysDict', '/system/sys/dict/index', NULL, 3, 0, '数据字典', 0, '2023-10-24 13:57:47', 'admin', 'admin', '2024-09-10 17:11:34', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('aeae8c48610543cbb3351797c9bdd1c4', '80e863628d5f453981dcbb853a3ef953', 1, 'sysTenant', '租户管理', 'tenant', 'sysTenant', '/system/sys/tenant/index', NULL, 5, 0, '租户管理', 0, '2023-10-24 13:58:15', 'admin', 'admin', '2024-09-10 17:16:01', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('d273f2b2f3cf4bfebb487a0634612904', '80e863628d5f4539814cbb853a3ef953', 1, 'baseDept', '部门管理', 'dept', 'baseDept', '/system/base/dept/index', NULL, 2, 0, NULL, 0, '2023-10-24 01:48:33', 'admin', 'admin', '2024-09-10 17:23:54', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('e301a2393802ea77799d74b55b871467', '80e863628d5f4539814cbb853a3ef953', 1, 'message', '系统消息', 'message', '/message', '/system/base/message/index', NULL, 0, 0, NULL, 0, '2023-10-24 13:56:24', 'admin', 'admin', '2024-09-10 17:26:19', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('506c4882ae29d353790b20b2ac942229', '21bfe17560284601a7c1b88d6b01f607', 1, 'userMessageDetails', '用户消息详情', NULL, '/user/message/details', '/center/message/MessageDetails', NULL, 4, 1, NULL, 0, '2024-09-29 10:53:44', 'admin', 'admin', '2024-09-29 11:12:40', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('72a797e1d59f0499396ef72d55d82a7d', '21bfe17560284601a7c1b88d6b01f607', 1, 'userMessage', '用户消息', 'el-icon-Bell', '/user/message', '/center/message/index', NULL, 3, 0, '用户的消息', 0, '2024-09-29 11:08:44', 'admin', 'admin', '2024-09-29 11:10:48', 'admin', 'admin', 'admin');

-- ----------------------------
-- Table structure for sys_menu_button
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_button`;
CREATE TABLE `sys_menu_button`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单id',
  `button_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '按钮编码',
  `button_title` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '按钮标题',
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求地址',
  `method` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求方式（POST|GET|DELETE|PUT）',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单描述',
  `is_deleted` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除(1:删除|0:未删除)',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime(0) NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `button_code`(`button_code`) USING BTREE COMMENT '按钮编码'
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统基础菜单按钮表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu_button
-- ----------------------------
INSERT INTO `sys_menu_button` VALUES (7, '5ed6df281aa2492cb9f026bc393db58d', 'addUser', '添加用户', '/api/admin/baseUsers', 'POST', '添加单个用户', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (8, '5ed6df281aa2492cb9f026bc393db58d', 'delUser', '删除用户', '/api/admin/baseUsers/*', 'DELETE', '删除单个用户', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (9, '5ed6df281aa2492cb9f026bc393db58d', 'upUser', '更新用户', '/api/admin/baseUsers/*', 'PUT', '更新单个用户', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (10, '5ed6df281aa2492cb9f026bc393db58d', 'userList', '用户列表', '/api/admin/baseUsers*', 'GET', '用户列表', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (11, '5ed6df281aa2492cb9f026bc393db58d', 'getUser', '查询用户', '/api/admin/baseUsers/**', 'GET', '查询单个用户', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (12, '5ed6df281aa2492cb9f026bc393db58d', 'baseUserInfo', '用户基本信息', '/api/admin/baseUsers/info', 'GET', '获取用户基本信息', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (13, '5ed6df281aa2492cb9f026bc393db58d', 'baseUserPermission', '用户权限', '/api/admin/baseUsers/permission', 'GET', '用户权限', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (14, '5ed6df281aa2492cb9f026bc393db58d', 'upUserRole', '更新角色', '/api/admin/baseUsers/*/role', 'PUT', '用户角色', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (15, 'a1a70a12612d4330b01268ba4f63e068', 'upPassword', '修改密码', '/api/admin/baseUsers/password', 'PUT', '个人中心修改密码', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (16, '49992d462a004f6d95d53a6e95fa6071', 'roleTree', '角色列表树', '/api/admin/sysRoles/tree', 'GET', '角色列表树', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (17, '5ed6df281aa2492cb9f026bc393db58d', 'getUserRole', '查询用户角色', '/api/admin/baseUsers/*/role', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (18, 'd273f2b2f3cf4bfebb487a0634612904', 'deptTree', '部门列表树', '/api/admin/baseDepts/tree*', 'GET', '部门列表树', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (19, 'd273f2b2f3cf4bfebb487a0634612904', 'addDept', '添加部门', '/api/admin/baseDepts', 'POST', '', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (20, 'd273f2b2f3cf4bfebb487a0634612904', 'getDept', '查询部门', '/api/admin/baseDepts/*', 'GET', '单个部门查询', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (21, 'd273f2b2f3cf4bfebb487a0634612904', 'delDept', '删除部门', '/api/admin/baseDepts/*', 'DELETE', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (22, 'd273f2b2f3cf4bfebb487a0634612904', 'upDept', '更新部门', '/api/admin/baseDepts/*', 'PUT', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (23, '224767b51e954e3299a49a92913c3128', 'getDeptUser', '部门用户列表', '/api/admin/baseUsers/deptCode/**', 'GET', '部门对应的用户列表', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (24, '224767b51e954e3299a49a92913c3128', 'upDeptUser', '更新部门用户', '/api/admin/baseDepts/deptUsers/*', 'PUT', '更新部门用户关系', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (25, '224767b51e954e3299a49a92913c3128', 'delDeptUser', '删除部门用户', '/api/admin/baseDepts/deptUsers/*/userId/*', 'DELETE', '删除部门用户关系', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (26, '01549dbf3c6542b99c7559f5d2883bdc', 'getMenuTree', '菜单树列表', '/api/admin/sysMenus/tree*', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (27, '01549dbf3c6542b99c7559f5d2883bdc', 'addMenu', '添加菜单', '/api/admin/sysMenus', 'POST', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (28, '01549dbf3c6542b99c7559f5d2883bdc', 'getMenu', '查询菜单', '/api/admin/sysMenus/**', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (29, '01549dbf3c6542b99c7559f5d2883bdc', 'delMenu', '删除菜单', '/api/admin/sysMenus/*', 'DELETE', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (30, '01549dbf3c6542b99c7559f5d2883bdc', 'menuButtonList', '菜单按钮列表', '/api/admin/sysMenuButtons*', 'GET', '查询菜单按钮列表', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (31, '01549dbf3c6542b99c7559f5d2883bdc', 'upMenu', '更新菜单', '/api/admin/sysMenus/*', 'PUT', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (32, '01549dbf3c6542b99c7559f5d2883bdc', 'addMenuButton', '添加菜单按钮', '/api/admin/sysMenuButtons', 'POST', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (33, '01549dbf3c6542b99c7559f5d2883bdc', 'upMenuButton', '更新菜单按钮', '/api/admin/sysMenuButtons/*', 'PUT', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (34, '01549dbf3c6542b99c7559f5d2883bdc', 'delMenuButton', '删除菜单按钮', '/api/admin/sysMenuButtons/*', 'DELETE', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (35, '441accbd16e945b496e0b66cc0d6f187', '35_null', '333', 'admin', 'GET', 'admin', 1, '2024-08-05 00:43:59', '2', '测试用户', '2024-08-05 00:43:59', '2', '测试用户', 'admin');
INSERT INTO `sys_menu_button` VALUES (36, '01549dbf3c6542b99c7559f5d2883bdc', 'getMenuButton', '查询菜单按钮', '/api/admin/sysMenuButtons/*', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (37, 'a789477a626041bd86aecbd2ae995d16', 'dictTypeTree', '字典类型树', '/api/admin/sysDictTypes/tree', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (38, 'a789477a626041bd86aecbd2ae995d16', 'getDictType', '查询字段类型', '/api/admin/sysDictTypes/*', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (39, 'a789477a626041bd86aecbd2ae995d16', 'addDictType', '添加字典类型', '/api/admin/sysDictTypes', 'POST', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (40, 'a789477a626041bd86aecbd2ae995d16', 'upDictType', '更新字典类型', '/api/admin/sysDictTypes/*', 'PUT', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (41, 'a789477a626041bd86aecbd2ae995d16', 'delDictType', '删除字典类型', '/api/admin/sysDictTypes/*', 'DELETE', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (42, 'a789477a626041bd86aecbd2ae995d16', 'getDictValue', '查询字典值', '/api/admin/sysDictValues/**', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (43, 'a789477a626041bd86aecbd2ae995d16', 'delDictValue', '删除字典值', '/api/admin/sysDictValues/*', 'DELETE', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (44, 'a789477a626041bd86aecbd2ae995d16', 'addDictValue', '添加字典值', '/api/admin/sysDictValues', 'POST', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (45, 'a789477a626041bd86aecbd2ae995d16', 'upDictValue', '更新字典值', '/api/admin/sysDictValues/*', 'PUT', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (46, '49992d462a004f6d95d53a6e95fa6071', 'addRole', '添加角色', '/api/admin/sysRoles', 'POST', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (47, '49992d462a004f6d95d53a6e95fa6071', 'getRole', '查询角色', '/api/admin/sysRoles/*', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (48, '49992d462a004f6d95d53a6e95fa6071', 'delRole', '删除角色', '/api/admin/sysRoles/*', 'DELETE', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (49, '49992d462a004f6d95d53a6e95fa6071', 'upRole', '更新角色', '/api/admin/sysRoles/*', 'PUT', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (50, '49992d462a004f6d95d53a6e95fa6071', 'getRoleMenuAuth', '查询授权菜单', '/api/admin/sysRoleAuthorizations/roleCode/*/menus', 'GET', '查询授权菜单列表', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (51, '49992d462a004f6d95d53a6e95fa6071', 'getRoleButtonAuth', '查询授权按钮', '/api/admin/sysRoleAuthorizations/*/resourceType/*', 'GET', '查询授权按钮列表', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (52, '49992d462a004f6d95d53a6e95fa6071', 'addRoleAuth', '授权角色权限', '/api/admin/sysRoleAuthorizations/*', 'POST', '添加角色权限：按钮权限、菜单权限', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (53, 'aeae8c48610543cbb3351797c9bdd1c4', 'tenants', '租户列表', '/api/admin/sysTenants*', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (54, 'aeae8c48610543cbb3351797c9bdd1c4', 'getTenant', '查询租户', '/api/admin/sysTenants/*', 'GET', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (55, 'aeae8c48610543cbb3351797c9bdd1c4', 'addTenant', '添加租户', '/api/admin/sysTenants', 'POST', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (56, 'aeae8c48610543cbb3351797c9bdd1c4', 'delTenant', '删除租户', '/api/admin/sysTenants/*', 'DELETE', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');
INSERT INTO `sys_menu_button` VALUES (57, 'aeae8c48610543cbb3351797c9bdd1c4', 'upTenant', '更新租户', '/api/admin/sysTenants/*', 'PUT', NULL, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `parent_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'root' COMMENT '父级角色id',
  `role_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码',
  `role_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `is_deleted` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除(1:删除|0:未删除)',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime(0) NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `role_code`(`role_code`) USING BTREE COMMENT '角色编码'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统基础角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('45378c752c844e03b293cb8181e3a71f', 'root', 'default', '默认角色', '默认角色', 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');

-- ----------------------------
-- Table structure for sys_role_authorization
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_authorization`;
CREATE TABLE `sys_role_authorization`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码',
  `resource_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源id',
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单按钮对应的菜单编码',
  `resource_type` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '资源类型（0菜单|1按钮）',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 481 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统角色授权关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_authorization
-- ----------------------------
INSERT INTO `sys_role_authorization` VALUES (15, 'default', '15', 'centerPersonal', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (63, 'default', '12', 'baseUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (64, 'default', '13', 'baseUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (65, 'default', '11', 'baseUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (66, 'default', '10', 'baseUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (67, 'default', '9', 'baseUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (68, 'default', '8', 'baseUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (69, 'default', '7', 'baseUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (70, 'default', '14', 'baseUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (71, 'default', '17', 'baseUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (94, 'default', '22', 'baseDept', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (95, 'default', '21', 'baseDept', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (96, 'default', '20', 'baseDept', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (97, 'default', '19', 'baseDept', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (98, 'default', '18', 'baseDept', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (109, 'default', '23', 'systemDeptUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (110, 'default', '24', 'systemDeptUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (111, 'default', '25', 'systemDeptUser', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (311, 'default', '39', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (312, 'default', '37', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (313, 'default', '38', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (314, 'default', '41', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (315, 'default', '42', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (316, 'default', '40', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (317, 'default', '45', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (318, 'default', '39', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (319, 'default', '44', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (320, 'default', '38', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (321, 'default', '42', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (322, 'default', '45', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (323, 'default', '44', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (324, 'default', '43', 'sysDict', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (369, 'default', '26', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (370, 'default', '30', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (371, 'default', '28', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (372, 'default', '27', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (373, 'default', '29', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (374, 'default', '31', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (375, 'default', '34', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (376, 'default', '33', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (377, 'default', '32', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (378, 'default', '36', 'sysMenu', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (403, 'default', '21bfe17560284601a7c1b88d6b01f607', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (404, 'default', '1422833c6ab044f5b89261bace41ad45', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (405, 'default', 'a1a70a12612d4330b01268ba4f63e068', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (406, 'default', '80e863628d5f4539814cbb853a3ef953', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (407, 'default', '5ed6df281aa2492cb9f026bc393db58d', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (408, 'default', 'd273f2b2f3cf4bfebb487a0634612904', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (409, 'default', '224767b51e954e3299a49a92913c3128', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (410, 'default', '80e863628d5f453981dcbb853a3ef953', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (411, 'default', '01549dbf3c6542b99c7559f5d2883bdc', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (412, 'default', 'a789477a626041bd86aecbd2ae995d16', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (413, 'default', '49992d462a004f6d95d53a6e95fa6071', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (414, 'default', 'aeae8c48610543cbb3351797c9bdd1c4', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (415, 'default', '49f5a59dfe5a4f22832052e7ad0b9f48', NULL, 0, 'admin');
INSERT INTO `sys_role_authorization` VALUES (458, 'default', '16', 'sysRole', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (459, 'default', '50', 'sysRole', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (460, 'default', '51', 'sysRole', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (461, 'default', '52', 'sysRole', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (462, 'default', '47', 'sysRole', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (463, 'default', '48', 'sysRole', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (464, 'default', '49', 'sysRole', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (465, 'default', '46', 'sysRole', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (476, 'default', '53', 'sysTenant', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (477, 'default', '57', 'sysTenant', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (478, 'default', '55', 'sysTenant', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (479, 'default', '54', 'sysTenant', 1, 'admin');
INSERT INTO `sys_role_authorization` VALUES (480, 'default', '56', 'sysTenant', 1, 'admin');

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `tenant_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '租户编码',
  `tenant_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '租户名称',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户说明',
  `is_deleted` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除(1:删除|0:未删除)',
  `is_super_tenant` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否超级租户（1是|0否）',
  `create_time` datetime(0) NOT NULL COMMENT '创建日期',
  `create_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户Id',
  `create_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户姓名',
  `update_time` datetime(0) NOT NULL COMMENT '最后更新日期',
  `update_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户Id',
  `update_user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新用户姓名',
  `tenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tenant_code`(`tenant_code`) USING BTREE COMMENT '租户编码'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统基础租户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES ('admin', 'root', '基础权限系统', '', 0, 0, '2024-08-05 00:43:59', 'admin', 'admin', '2024-08-05 00:43:59', 'admin', 'admin', 'admin');

SET FOREIGN_KEY_CHECKS = 1;
