# cslc-admin-ui

权限系统前端项目
是基于 Vue3 + Vite5+ TypeScript5 + Element-Plus + Pinia 等主流技术栈构建的免费开源的-前端模板

## 一、目录模块说明
TODO
## 二、开发环境搭建
### 2.1 环境配置

| 环境                 | 名称版本                                                     | 下载地址                                                     |
| -------------------- | :----------------------------------------------------------- | ------------------------------------------------------------ |
| **开发工具**         | VSCode    | [下载](https://code.visualstudio.com/Download)           |
| **运行环境**         | Node ≥18 (其中 20.6.0 版本不可用)    | [下载](http://nodejs.cn/download)                        |


编辑器打开项目，执行以下命令做项目初始化
```bash
# 进入项目目录
# cd cslc-admin-ui

# 配置淘宝源，提高下载速度（可选）
npm config set registry https://registry.npmmirror.com

## 安装 pnpm
npm install pnpm -g

## 异常
1.问题：   
pnpm : 无法加载文件 xxxxx \npm\pnpm.ps1，因为在此系统上禁止运行脚
2.解决方案：
以管理员身份运行 PowerShell: Set-ExecutionPolicy RemoteSigned

## 安装依赖
pnpm install

## 启动运行
pnpm run dev

```

## 三、项目发布说明
通过执行打包命令后，在当前项目根目录生成dist的文件夹，部署到云服务器的nginx中
```
# 构建生产环境
pnpm run build:prod

# 上传文件至远程服务器
将打包生成在 `dist` 目录下的文件拷贝至 `/xxx/ui` 目录

# nginx.cofig 配置
server {
	listen     80;
	server_name  localhost;
	location / {
			root /xxx/ui;
			index index.html index.htm;
	}
	# 反向代理配置
	location /prod-api/ {
      # api.xxx.com 替换后端API地址，注意保留后面的斜杠 /
      proxy_pass http://api.xxx.com/; 
	}
}
```

## 四、VSCode环境配置（可选）
#### 4.1 Vetur格式化配置
```
{
    "vetur.format.defaultFormatterOptions": {
        "prettier": {
            // 语句末尾不加分号
            "semi": false,
            // 用单引号
            "singleQuote": true,
            // 禁止随时添加逗号
            "trailingComma": "none",
        }
    }
}
```
#### 4.2 editorConfig
不同的编辑器和 IDE 中保持一致的编码样式，有些编辑器默认支持editorConfig，如webstorm；而有些编辑器则需要安装editorConfig插件，如ATOM、Sublime、VS Code等

