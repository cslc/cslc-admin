import axios, { InternalAxiosRequestConfig, AxiosResponse } from "axios";
import { useUserStoreHook } from "@/store/modules/user";
import { ResultEnum } from "@/enums/ResultEnum";
import { TOKEN_KEY } from "@/enums/CacheEnum";

// 创建 axios 实例
const service = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API,
  timeout: 50000,
  headers: {
    "Content-Type": "application/json;charset=utf-8",
    // 在cdn中不缓存api请求
    "Cache-Control": "no-cache, no-store, must-revalidate",
  },
});

// 请求拦截器
service.interceptors.request.use(
  (config: InternalAxiosRequestConfig) => {
    const accessToken = localStorage.getItem(TOKEN_KEY);
    if (accessToken) {
      config.headers.Authorization = accessToken;
    }
    return config;
  },
  (error: any) => {
    return Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  (response: AxiosResponse) => {
    // 检查配置的响应类型是否为二进制类型（'blob' 或 'arraybuffer'）, 如果是，直接返回响应对象
    if (
      response.config.responseType === "blob" ||
      response.config.responseType === "arraybuffer"
    ) {
      return response;
    }
    // response.data 返回的api接口数据
    const { status, message } = response.data;
    if (status === ResultEnum.SUCCESS) {
      return response.data;
    }

    ElMessage.error(message || "系统内部异常");
    return Promise.reject(new Error(message || "Error"));
  },
  (error: any) => {
    // 异常处理
    if (error.response.data) {
      const { status, message } = error.response.data;
      if (status === ResultEnum.TOKEN_INVALID) {
        ElNotification({
          title: "提示",
          message: message || "用户授权过期或者其他端已登出,请重新登陆!",
          type: "info",
        });
        // 清空token
        useUserStoreHook()
          .resetToken()
          .then(() => {
            location.reload();
          });
      } else if (status === ResultEnum.ERROR) {
        ElNotification({
          title: "提示",
          message: message || "系统内部异常!",
          type: "error",
        });
      } else if (status === ResultEnum.EMPTY) {
        ElNotification({
          title: "提示",
          message: "请求不存在，请检查入参!",
          type: "error",
        });
      } else {
        ElMessage.error(message || "系统内部异常!");
      }
    }
    return Promise.reject(error.message);
  }
);

// 导出 axios 实例
export default service;
