/**
 * 全局可直接使用的接口类型
 */
declare global {
  /**
   * api请求单个响应体
   */
  interface ObjectResultResponse<T extends object> {
    /** 请求返回状态码 200 成功 */
    status: number;
    /** 请求返回消息 */
    message?: string;
    data: T;
    metadata: any;
  }
  /**
   * api请求表格数据
   */
  interface TableData<T extends object> {
    /** 请求返回数据集 */
    rows: T[];
    /** 请求返回数据总数 */
    total: number;
  }

  /**
   * api请求表格响应体
   */
  interface TableResultResponse<T extends object> {
    /** 请求返回状态码 200 成功 */
    status: number;
    /** 请求返回消息 */
    message?: string;
    data: TableData<T>;
    metadata: any;
  }

  /**
   * api请求表格翻页请求入参
   */
  interface TableRequestPage {
    /** 当前页码 */
    page?: number;
    /** 查询条数 */
    limit?: number;
  }

  /**
   * 响应VO基础字段
   */
  interface BaseVO {
    /** 是否删除 */
    isDeleted?: number;
    /** 创建日期 */
    createTime?: string;
    /** 创建人id */
    createUserId?: string;
    /** 创建人 */
    createUserName?: string;
    /** 更新日期 */
    updateTime?: string;
    /** 更新用户id */
    updateUserId?: string;
    /** 更新用户 */
    updateUserName?: string;
    /** 租户id */
    tenantId?: string;
  }

  /**
   * 页签对象
   */
  interface TagView {
    /** 页签名称 */
    name: string;
    /** 页签标题 */
    title: string;
    /** 页签路由路径 */
    path: string;
    /** 页签路由完整路径 */
    fullPath: string;
    /** 页签图标 */
    icon?: string;
    /** 是否固定页签 */
    affix?: boolean;
    /** 是否开启缓存 */
    keepAlive?: boolean;
    /** 路由查询参数 */
    query?: any;
  }

  /**
   * 系统设置
   */
  interface AppSettings {
    /** 系统标题 */
    title: string;
    /** 系统版本 */
    version: string;
    /** 是否显示设置 */
    showSettings: boolean;
    /** 是否固定头部 */
    fixedHeader: boolean;
    /** 是否显示多标签导航 */
    tagsView: boolean;
    /** 是否显示侧边栏Logo */
    sidebarLogo: boolean;
    /** 导航栏布局(left|top|mix) */
    layout: string;
    /** 主题颜色 */
    themeColor: string;
    /** 主题模式(dark|light) */
    theme: string;
    /** 布局大小(default |large |small) */
    size: string;
    /** 语言( zh-cn| en) */
    language: string;
    /** 是否开启水印 */
    watermarkEnabled: boolean;
    /** 水印内容 */
    watermarkContent: string;
  }

  /**
   * 组件数据源
   */
  interface OptionType {
    /** 值 */
    value: string | number;
    /** 文本 */
    label: string;
    /** 子列表  */
    children?: OptionType[];
  }
}
export {};
