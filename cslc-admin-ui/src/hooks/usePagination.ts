import { reactive } from "vue"

interface DefaultPaginationData {
  total: number
  page: number
  pageSizes: number[]
  limit: number
  layout: string
}

interface PaginationData {
  total?: number
  page?: number
  pageSizes?: number[]
  limit?: number
  layout?: string
}

/** 默认的分页参数 */
const defaultPaginationData: DefaultPaginationData = {
  total: 0,
  page: 1,
  pageSizes: [10, 20, 50],
  limit: 10,
  layout: "total, sizes, prev, pager, next, jumper"
}

export function usePagination(initialPaginationData: PaginationData = {}) {
  /** 合并分页参数 */
  const paginationData = reactive({ ...defaultPaginationData, ...initialPaginationData })
  /** 改变当前页码 */
  const handleCurrentChange = (value: number) => {
    paginationData.page = value
  }
  /** 改变页面大小 */
  const handleSizeChange = (value: number) => {
    paginationData.limit = value
  }

  return { paginationData, handleCurrentChange, handleSizeChange }
}
