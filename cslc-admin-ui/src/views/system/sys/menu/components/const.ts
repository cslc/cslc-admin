/** 系统内置图标 */
export const ICON_OPTIONS = [
  { value: "api", label: "api" },
  { value: "backtop", label: "backtop" },
  { value: "captcha", label: "captcha" },
  { value: "cascader", label: "cascader" },
  { value: "chart", label: "chart" },
  { value: "client", label: "client" },
  { value: "close", label: "close" },
  { value: "close_all", label: "close_all" },
  { value: "close_left", label: "close_left" },
  { value: "close_other", label: "close_other" },
  { value: "close_right", label: "close_right" },
  { value: "component", label: "component" },
  { value: "dashboard", label: "dashboard" },
  { value: "dict", label: "dict" },
  { value: "document", label: "document" },
  { value: "download", label: "download" },
  { value: "edit", label: "edit" },
  { value: "example", label: "example" },
  { value: "eye-open", label: "eye-open" },
  { value: "eye", label: "eye" },
  { value: "form", label: "form" },
  { value: "fullscreen-exit", label: "fullscreen-exit" },
  { value: "fullscreen", label: "fullscreen" },
  { value: "github", label: "github" },
  { value: "homepage", label: "homepage" },
  { value: "indent-decrease", label: "indent-decrease" },
  { value: "ip", label: "ip" },
  { value: "language", label: "language" },
  { value: "link", label: "link" },
  { value: "list", label: "list" },
  { value: "lock", label: "lock" },
  { value: "menu", label: "menu" },
  { value: "message", label: "message" },
  { value: "money", label: "money" },
  { value: "monitor", label: "monitor" },
  { value: "moon", label: "moon" },
  { value: "nested", label: "nested" },
  { value: "order", label: "order" },
  { value: "password", label: "password" },
  { value: "people", label: "people" },
  { value: "peoples", label: "peoples" },
  { value: "project", label: "project" },
  { value: "publish", label: "publish" },
  { value: "refresh", label: "refresh" },
  { value: "role", label: "role" },
  { value: "security", label: "security" },
  { value: "settings", label: "settings" },
  { value: "size", label: "size" },
  { value: "sunny", label: "sunny" },
  { value: "system", label: "system" },
  { value: "table", label: "table" },
  { value: "todolist", label: "todolist" },
  { value: "tree-table", label: "tree-table" },
  { value: "tree", label: "tree" },
  { value: "user", label: "user" },
  { value: "visit", label: "visit" },
  { value: "subscribe", label: "subscribe" },
  { value: "payment", label: "payment" },
  { value: "payment-history", label: "payment-history" },
  { value: "tenant", label: "tenant" },
  { value: "swagger", label: "swagger" },
  { value: "dept", label: "dept" },
  { value: "dept-user", label: "dept-user" },
  { value: "level", label: "level" },
];

/** 菜单类型 */
export const MENU_TYPE_OPTIONS = [
  {
    value: 0,
    label: "目录",
  },
  {
    value: 1,
    label: "菜单",
  },
];

/** 菜单隐藏 */
export const MENU_HIDDEN_OPTIONS = [
  {
    value: 0,
    label: "显示",
  },
  {
    value: 1,
    label: "隐藏",
  },
];

/** 请求方式 */
export const METHOD_OPTIONS = [
  {
    value: "POST",
    label: "POST",
  },
  {
    value: "PUT",
    label: "PUT",
  },
  {
    value: "DELETE",
    label: "DELETE",
  },
  {
    value: "GET",
    label: "GET",
  },
];
