/* 通知类型枚举 */
export const enum NoticeTypeEnum {
  /** 系统升级 */
  SYSTEM_UPGRADE = "SYSTEM_UPGRADE",
  /** 系统维护 */
  SYSTEM_MAINTENANCE = "SYSTEM_MAINTENANCE",
  /** 安全警告 */
  SECURITY_ALERT = "SECURITY_ALERT",
  /** 假期通知 */
  HOLIDAY_NOTICE = "HOLIDAY_NOTICE",
  /** 公司新闻 */
  COMPANY_NEWS = "COMPANY_NEWS",
  /** 其他通知 */
  OTHER = "OTHER",
}

// 定义标签映射
const NoticeTypeLabels: Record<string, string> = {
  SYSTEM_UPGRADE: "系统升级",
  SYSTEM_MAINTENANCE: "系统维护",
  SECURITY_ALERT: "安全警告",
  HOLIDAY_NOTICE: "假期通知",
  COMPANY_NEWS: "公司新闻",
  OTHER: "其他通知",
};

// 导出获取标签函数
export const getNoticeLabel = (type: string): string => {
  return NoticeTypeLabels[type] || "";
};
