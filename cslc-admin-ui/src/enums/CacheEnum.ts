/**
 * 令牌缓存Key
 */
export const TOKEN_KEY = "accessToken";

/**
 * 路由菜单
 */
export const MENU_TREE_KEY = "menuTree";

/**
 * 菜单按钮
 */
export const MENU_BUTTON_KEY = "menuButton";