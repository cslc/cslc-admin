/**
 * 公共全局常量
 */

/** 用户性别 */
export const SEX_OPTIONS = [
  {
    value: 0,
    label: "男",
  },
  {
    value: 1,
    label: "女",
  },
];
/** 禁用启动 */
export const DISABLED_OPTIONS = [
  {
    value: 0,
    label: "正常",
  },
  {
    value: 1,
    label: "禁用",
  },
];

/** 菜单资源类型 */
export const MENU_RESOURCE_TYPE = {
  /** 目录 */
  menu: 0,
  /** 菜单 */
  button: 1,
};

/** 卡卷类型 */
export const CARD_TYPE_OPTIONS = [
  {
    value: "number",
    label: "消费次卡",
  },
];
/** 会员折扣 */
export const DISCOUNT_OPTIONS = [
  { label: "无折扣", value: 0 },
  { label: "6折", value: 0.6 },
  { label: "65折", value: 0.65 },
  { label: "7折", value: 0.7 },
  { label: "75折", value: 0.75 },
  { label: "8折", value: 0.8 },
  { label: "85折", value: 0.85 },
  { label: "88折", value: 0.88 },
  { label: "9折", value: 0.9 },
  { label: "95折", value: 0.95 },
];

/** 支付方式 */
export const PAYMENT_TYPE_OPTIONS = [
  {
    value: "card",
    label: "次卡消费",
  },
  {
    value: "balance",
    label: "余额消费",
  },
  {
    value: "other",
    label: "第三方支付",
  },
];
/** 第三方平台支付方式 */
export const OTHER_PAYMENT_TYPE_OPTIONS = [
  {
    value: "wx",
    label: "微信",
  },
  {
    value: "zfb",
    label: "支付宝",
  },
  {
    value: "xj",
    label: "现金",
  },
  {
    value: "dy",
    label: "抖音",
  },
  {
    value: "mt",
    label: "美团",
  },
  {
    value: "elm",
    label: "饿了么",
  },
  {
    value: "xhs",
    label: "小红书",
  },
];
/** 消息通知类型 */
export const MESSAGE_NOTICE_TYPE = [
  {
    value: "SYSTEM_UPGRADE",
    label: "系统升级",
  },
  {
    value: "SYSTEM_MAINTENANCE",
    label: "系统维护",
  },
  {
    value: "SECURITY_ALERT",
    label: "安全警告",
  },
  {
    value: "HOLIDAY_NOTICE",
    label: "假期通知",
  },
  {
    value: "COMPANY_NEWS",
    label: "公司新闻",
  },
  {
    value: "OTHER",
    label: "其他通知",
  }
];

/**
 * 通过value获取对象
 */
export const getLabelByValue = (key: any, data: any) => {
  if (key != undefined && key != null) {
    return data?.find((item: any) => item.value === key).label;
  } else {
    return key;
  }
};
