import request from "@/utils/request";

class UserAPI {
  /**
   * 获取当前登录用户信息
   *
   * @returns 登录用户昵称、头像信息，包括角色和权限
   */
  static getInfo() {
    return request<any, ObjectResultResponse<UserInfo>>({
      url: "/admin/baseUsers/info",
      method: "get",
    });
  }
}

export default UserAPI;

/** 登录用户信息 */
export interface UserInfo {
  baseUser: BaseUser;
}

/** 用户基础信息 */
export interface BaseUser {
  /** 用户id */
  id: string;

  /** 用户账号 */
  account: string;

  /** 用户名称 */
  userName: string;

  /** 头像URL */
  portrait?: string;

  /** 手机号 */
  mobilePhone?: string;

  /** 邮箱 */
  userEmail?: string;

   /** 性别 */
  userSex?: number;
}
