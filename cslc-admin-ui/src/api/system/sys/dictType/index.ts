import request from "@/utils/request";

/** 字典类型API  */
class DictTypeAPI {
  /**
   * 获取字典类型树
   */
  static getDictTypeTreeApi() {
    return request<any, ObjectResultResponse<DictTypeVO[]>>({
      url: "/admin/sysDictTypes/tree",
      method: "get",
    });
  }

  /**
   * 通过主键id获取信息
   * @param {String} id 主键
   */
  static getDictTypeByIdApi(id: string) {
    return request<any, ObjectResultResponse<DictTypeVO>>({
      url: `/admin/sysDictTypes/${id}`,
      method: "get",
    });
  }

  /**
   * 通过id更新实体
   * @param {*} id 主键
   * @param {*} obj 实体信息
   */
  static putSysDictTypeApi(id: string, obj: DictTypeVO) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysDictTypes/" + id,
      method: "put",
      data: obj,
    });
  }

  /**
   * 通过主键删除实体
   * @param {*} id 主键
   */
  static delSysDictTypeApi(id: string) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysDictTypes/" + id,
      method: "delete",
    });
  }

  /**
   * 添加新实体
   * @param {*} obj 实体信息
   */
  static addSysDictTypeApi(obj: DictTypeVO) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysDictTypes",
      method: "post",
      data: obj,
    });
  }
}

export default DictTypeAPI;


/** 查询条件 */
export interface TableRequestParams extends TableRequestPage {}
/** 字典类型 */
export interface DictTypeVO extends BaseVO {
  id?: string;
  /** 父id */
  parentId?: string;
  /** 类型名称 */
  dictTypeName?: string;
  /** 类型编码 */
  dictTypeCode?: string;
  /** 排序 */
  orderNum?: number;
  /** 字典类型说明 */
  description?: string;
  /** 子集 */
  children?: DictTypeVO[];

  /** 是否展开 */
  expand?: boolean;
  /** 展示名称 */
  label?: string;
}

