import request from "@/utils/request";

/** 租户API */
class TenantAPI {
  /**
   * 查询列表
   * @param {*} query 查询条件
   */
  static getSysTenantListApi(query: TableRequestParams) {
    return request<any, TableResultResponse<TenantVO>>({
      url: "/admin/sysTenants",
      method: "get",
      params: query,
    });
  }
  /**
   * 添加新实体
   * @param {*} obj 实体信息
   */
  static addSysTenantApi(obj: TenantVO) {
    return request<any, ObjectResultResponse<TenantVO>>({
      url: "/admin/sysTenants",
      method: "post",
      data: obj,
    });
  }
  /**
   * 通过主键获取实体详情
   * @param {*} id 主键
   */
  static getSysTenantApi(id: string) {
    return request<any, ObjectResultResponse<TenantVO>>({
      url: "/admin/sysTenants/" + id,
      method: "get",
    });
  }
  /**
   * 通过主键删除实体
   * @param {*} id 主键
   */
  static delSysTenantApi(id: string) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysTenants/" + id,
      method: "delete",
    });
  }
  /**
   * 通过id更新实体
   * @param {*} id 主键
   * @param {*} obj 实体信息
   */
  static putSysTenantApi(id: string, obj: TenantVO) {
    return request<any, ObjectResultResponse<TenantVO>>({
      url: "/admin/sysTenants/" + id,
      method: "put",
      data: obj,
    });
  }
}

export default TenantAPI;

/** 翻页查询条件 */
export interface TableRequestParams extends TableRequestPage {
  /** 租户名称 */
  tenantName?: string;
}
/** 字典值 */
export interface TenantVO extends BaseVO {
  /** id */
  id?: string;
  /** 租户编码 */
  tenantCode?: string;
  /** 租户名称 */
  tenantName?: string;
  /** 租户说明 */
  description?: string;
}
