import request from "@/utils/request";

/** 菜单按钮API */
class MenuButtonAPI {
  /**
   * 查询列表
   * @param {*} query 查询条件
   */
  static getSysMenuButtonListApi(query: any) {
    return request<any, TableResultResponse<MenuButtonVO>>({
      url: "/admin/sysMenuButtons",
      method: "get",
      params: query,
    });
  }
  /**
   * 添加新实体
   * @param {*} obj 实体信息
   */
  static addSysMenuButtonApi(obj: MenuButtonVO) {
    return request({
      url: "/admin/sysMenuButtons",
      method: "post",
      data: obj,
    });
  }
  /**
   * 通过主键获取实体详情
   * @param {*} id 主键
   */
  static getSysMenuButtonApi(id: number) {
    return request<any, ObjectResultResponse<MenuButtonVO>>({
      url: "/admin/sysMenuButtons/" + id,
      method: "get",
    });
  }
  /**
   * 通过主键删除实体
   * @param {*} id 主键
   */
  static delSysMenuButtonApi(id: number) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysMenuButtons/" + id,
      method: "delete",
    });
  }
  /**
   * 通过id更新实体
   * @param {*} id 主键
   * @param {*} obj 实体信息
   */
  static putSysMenuButtonApi(id: number, obj: MenuButtonVO) {
    return request<any, ObjectResultResponse<MenuButtonVO>>({
      url: "/admin/sysMenuButtons/" + id,
      method: "put",
      data: obj,
    });
  }
}

export default MenuButtonAPI;

export interface TableRequestParams extends TableRequestPage {
  /** 菜单id */
  menuId?: string;
}
/** 菜单按钮 */
export interface MenuButtonVO extends BaseVO {
  id?: number;
  menuId?: string;
  /** 按钮编码 */
  buttonCode?: string;
  /** 按钮标题 */
  buttonTitle?: string;
  /** 请求地址 */
  url?: string;
  /** 请求方式（POST|GET|DELETE|PUT） */
  method?: string;
  /** 菜单描述 */
  description?: string;
}
