import request from "@/utils/request";

/** 角色API */
class RoleAPI {
  /**
   * 查询列表
   * @param {*} query 查询条件
   */
  static getSysRoleListApi(query: TableRequestParams) {
    return request<any, TableResultResponse<RoleVO>>({
      url: "/admin/sysRoles",
      method: "get",
      params: query,
    });
  }
  /**
   * 添加新实体
   * @param {*} obj 实体信息
   */
  static addSysRoleApi(obj: RoleVO) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysRoles",
      method: "post",
      data: obj,
    });
  }
  /**
   * 通过主键获取实体详情
   * @param {*} id 主键
   */
  static getSysRoleApi(id: string) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysRoles/" + id,
      method: "get",
    });
  }
  /**
   * 通过主键删除实体
   * @param {*} id 主键
   */
  static delSysRoleApi(id: string) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysRoles/" + id,
      method: "delete",
    });
  }
  /**
   * 通过id更新实体
   * @param {*} id 主键
   * @param {*} obj 实体信息
   */
  static putSysRoleApi(id: string, obj: RoleVO) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysRoles/" + id,
      method: "put",
      data: obj,
    });
  }
}

export default RoleAPI;

/** 翻页查询条件 */
export interface TableRequestParams extends TableRequestPage {
  /** 查询参数：角色名 */
  roleName?: string;
}

/** 角色对象 */
export interface RoleVO extends BaseVO {
  id?: string;
  roleCode?: string;
  roleName?: string;
  description?: string;
}
