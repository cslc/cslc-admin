import request from "@/utils/request";

/** 字典值API */
class DictValueAPI {
  /**
   * 查询列表
   * @param {*} query 查询条件
   */
  static getSysDictValueListApi(query: TableRequestParams) {
    return request<any, TableResultResponse<DictValueVO>>({
      url: "/admin/sysDictValues",
      method: "get",
      params: query,
    });
  }
  /**
   * 添加字典值
   * @param {*} obj 实体信息
   */
  static addSysDictValueApi(obj: DictValueVO) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysDictValues",
      method: "post",
      data: obj,
    });
  }
  /**
   * 通过主键获取实体详情
   * @param {*} id 主键
   */
  static getSysDictValueApi(id: string) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysDictValues/" + id,
      method: "get",
    });
  }
  /**
   * 通过主键删除实体
   * @param {*} id 主键
   */
  static delSysDictValueApi(id: string) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysDictValues/" + id,
      method: "delete",
    });
  }
  /**
   * 通过id更字典值
   * @param {*} id 主键
   * @param {*} obj 实体信息
   */
  static putSysDictValueApi(id: string, obj: DictValueVO) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysDictValues/" + id,
      method: "put",
      data: obj,
    });
  }
  /**
   * 通过dictCode获取匹配集合
   * @param {*} dictCode code码
   */
  static getByDictCodeApi(dictCode: string) {
    return request<any, ObjectResultResponse<DictValueVO>>({
      url: `/admin/sysDictValues/dictCode/${dictCode}`,
      method: "get",
    });
  }
}

export default DictValueAPI;

/** 翻页查询条件 */
export interface TableRequestParams extends TableRequestPage {
  /** 字典类型id */
  typeId: string;
}
/** 字典值 */
export interface DictValueVO extends BaseVO {
  id?: string;
  /** 字典类型id */
  typeId: string;
  /** 值名称 */
  dictTitle?: string;
  /** 值编码 */
  dictCode?: string;
  /** 排序 */
  orderNum?: number;
  /** 字典值说明 */
  description?: string;
}
