import request from "@/utils/request";

/** 角色权限API */
class RoleAuthorizationAPI {
  /**
   * 角色授权接口，角色和菜单做关联
   * @param {String} roleCode 角色编码
   * @param {RoleAuthorizationVO} roleAuthorizationVO 角色授权
   */
  static setRoleAuthApi(
    roleCode: string,
    roleAuthorizationVO: RoleAuthorizationVO
  ) {
    return request({
      url: `/admin/sysRoleAuthorizations/${roleCode}`,
      method: "post",
      data: roleAuthorizationVO,
    });
  }
  /**
   * 获取指定角色的菜单权限
   * @param {String} roleCode 角色编码
   * @param {int} resourceType 资源类型
   */
  static getAuthByRoleCodeApi(roleCode: string, resourceType: number) {
    return request({
      url: `/admin/sysRoleAuthorizations/${roleCode}/resourceType/${resourceType}`,
      method: "get",
    });
  }
  /**
   * 获取角色对应的菜单权限集合
   * @param {String} roleCode 角色编码
   */
  static getMenuAuthByRoleCodeApi(roleCode: string) {
    return request({
      url: `/admin/sysRoleAuthorizations/roleCode/${roleCode}/menus`,
      method: "get",
    });
  }
}

export default RoleAuthorizationAPI;

/** 角色授权 */
export interface RoleAuthorizationVO {
  /** 菜单按钮对应的菜单编码 */
  menuCode?: string;
  /** 资源类型（0菜单|1按钮） */
  resourceType?: number;
  /** 资源id集合 */
  resourceIds?: string[];
}
