import request from "@/utils/request";
import type * as Menu from "./types";

class MenuAPI {
  /**
   * 添加新实体
   * @param {*} obj 实体信息
   */
  static addSysMenuApi(obj: Menu.MenuVO) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysMenus",
      method: "post",
      data: obj,
    });
  }
  /**
   * 通过主键获取实体详情
   * @param {*} id 主键
   */
  static getSysMenuApi(id: string) {
    return request<any, ObjectResultResponse<Menu.MenuVO>>({
      url: "/admin/sysMenus/" + id,
      method: "get",
    });
  }
  /**
   * 通过主键删除实体
   * @param {*} id 主键
   */
  static delSysMenuApi(id: string) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysMenus/" + id,
      method: "delete",
    });
  }
  /**
   * 通过id更新实体
   * @param {*} id 主键
   * @param {*} obj 实体信息
   */
  static putSysMenuApi(id: string, obj: Menu.MenuVO) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/sysMenus/" + id,
      method: "put",
      data: obj,
    });
  }
  /**
   * 获取菜单树
   */
  static getMenuTreesApi() {
    return request<any, ObjectResultResponse<Menu.MenuVO[]>>({
      url: "/admin/sysMenus/tree",
      method: "get",
    });
  }
}

export default MenuAPI;

/** 翻页查询条件 */
export interface TableRequestParams extends TableRequestPage {}
/** 菜单 */
export interface MenuVO extends BaseVO {
  id?: string;
  /** 菜单父级id */
  parentId?: string;
  /** 菜单父级标题 */
  parentMenuTitle?: string;
  /** (0:目录|1:菜单) */
  menuType?: number;
  /** 菜单编码 */
  menuCode?: string;
  /** 菜单标题 */
  menuTitle?: string;
  /** 菜单图标 */
  menuIcon?: string;
  /** 菜单路径（路径别名） */
  menuPath?: string;
  /** 菜单路径(菜单url地址) */
  component?: string;
  /** 页面组件(页面目录地址) */
  redirect?: string;
  /** 是否隐藏菜单（0不隐藏,1隐藏） */
  hidden?: number;
  /**排序 */
  orderNum?: number;
  /** 菜单描述 */
  description?: string;
  /** 子节点 */
  children?: MenuVO[];
}
