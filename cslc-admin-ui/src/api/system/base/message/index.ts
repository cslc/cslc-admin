import request from "@/utils/request";

class MessageAPI {
  /**
   * 查询列表
   * @param {*} query 查询条件
   */
  static getBaseMessageListApi(query: TableRequestParams) {
    return request<any, TableResultResponse<MessageVO>>({
      url: "/admin/base/message",
      method: "get",
      params: query,
    });
  }
  /**
   * 添加新实体
   * @param {*} obj 实体信息
   */
  static addBaseMessageApi(obj: MessageVO) {
    return request<any, ObjectResultResponse<MessageVO>>({
      url: "/admin/base/message",
      method: "post",
      data: obj,
    });
  }
  /**
   * 通过主键获取实体详情
   * @param {*} id 主键
   */
  static getBaseMessageApi(id: number) {
    return request<any, ObjectResultResponse<MessageVO>>({
      url: "/admin/base/message/" + id,
      method: "get",
    });
  }
  /**
   * 通过主键删除实体
   * @param {*} id 主键
   */
  static delBaseMessageApi(id: number) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/base/message/" + id,
      method: "delete",
    });
  }
  /**
   * 通过id更新实体
   * @param {*} id 主键
   * @param {*} obj 实体信息
   */
  static putBaseMessageApi(id: number, obj: MessageVO) {
    return request<any, ObjectResultResponse<MessageVO>>({
      url: "/admin/base/message/" + id,
      method: "put",
      data: obj,
    });
  }
}
export default MessageAPI;

/** 翻页查询条件 */
export interface TableRequestParams extends TableRequestPage {
  /** 查询参数：标题 */
  title?: string;
  /** 消息类型 */
  type?: string;
}

export interface MessageVO extends BaseVO {
  id?: number;
  /** 消息标题 */
  title?: string;
  /** 消息类型 */
  type?: string;
  /** 消息内容 */
  content?: string;
}
