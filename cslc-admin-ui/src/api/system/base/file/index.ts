import request from "@/utils/request";

/** 文件api */
class FileAPI {
  /**
   * 文件上传地址
   */
  static uploadUrl = import.meta.env.VITE_APP_BASE_API + "/oss/upload";
  /**
   * 查询列表
   * @param {*} query 查询条件
   */
  static getFileListApi(query: TableRequestParams) {
    return request<any, TableResultResponse<FileVO>>({
      url: "/base/file",
      method: "get",
      params: query,
    });
  }

  /**
   * 上传文件
   *
   * @param file
   */
  static upload(file: File) {
    const formData = new FormData();
    formData.append("file", file);
    return request<any, FileVO>({
      url: FileAPI.uploadUrl,
      method: "post",
      data: formData,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  }

  /**
   * 删除文件
   *
   * @param filePath 文件完整路径
   */
  static deleteByPath(filePath?: string) {
    return request({
      url: "",
      method: "delete",
      params: { filePath: filePath },
    });
  }

  /**
   * 下载文件
   * @param url
   * @param fileName
   */
  static downloadFile(url: string, fileName?: string) {
    return request({
      url: url,
      method: "get",
      responseType: "blob",
    }).then((res) => {
      const blob = new Blob([res.data]);
      const a = document.createElement("a");
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = fileName || "下载文件";
      a.click();
      window.URL.revokeObjectURL(url);
    });
  }
}
export default FileAPI;

/** 翻页参数 */
export interface TableRequestParams extends TableRequestPage {
  /** 查询参数：标题 */
  fileName?: string;
}

/** 文件对象 */
export interface FileVO extends BaseVO {
  id?: number;
  /** 文件唯一标志 */
  sign?: string;
  /** 文件名 */
  fileName?: string;
  /** 扩展名 */
  suffix?: string;
  /** 文件大小（单位:KB） */
  size?: string;
}
