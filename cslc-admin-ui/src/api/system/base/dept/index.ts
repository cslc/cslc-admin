import request from "@/utils/request";

/** 部门API */
class DeptAPI {
  /**
   * 查询列表
   * @param {*} query 查询条件
   */
  static getBaseDeptTreeApi(query?: TableRequestParams) {
    return request<any, ObjectResultResponse<DeptVO[]>>({
      url: "/admin/baseDepts/tree",
      method: "get",
      params: query,
    });
  }
  /**
   * 添加新实体
   * @param {*} obj 实体信息
   */
  static addBaseDeptApi(obj: DeptVO) {
    return request<any, ObjectResultResponse<DeptVO>>({
      url: "/admin/baseDepts",
      method: "post",
      data: obj,
    });
  }
  /**
   * 通过主键获取实体详情
   * @param {*} id 主键
   */
  static getBaseDeptApi(id: string) {
    return request<any, ObjectResultResponse<DeptVO>>({
      url: "/admin/baseDepts/" + id,
      method: "get",
    });
  }
  /**
   * 通过主键删除实体
   * @param {*} id 主键
   */
  static delBaseDeptApi(id: string) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/baseDepts/" + id,
      method: "delete",
    });
  }
  /**
   * 通过id更新实体
   * @param {*} id 主键
   * @param {*} obj 实体信息
   */
  static putBaseDeptApi(id: string, obj: DeptVO) {
    return request<any, ObjectResultResponse<DeptVO>>({
      url: "/admin/baseDepts/" + id,
      method: "put",
      data: obj,
    });
  }
  /**
   * 添加部门用户关系
   *
   * @param {String} deptCode 部门编码
   * @param {*} data 查询条件
   *
   */
  static addDeptUserApi(deptCode: string, data: DeptUserVO) {
    return request<any, ObjectResultResponse<any>>({
      url: `admin/baseDepts/deptUsers/${deptCode}`,
      method: "put",
      data: data,
    });
  }
  /**
   * 删除部门的用户关系
   *
   * @param {String} deptCode 部门编码
   * @param {String} userId 用户主键
   *
   */
  static delDeptUserByUserIdApi(deptCode: string, userId: string) {
    return request<any, ObjectResultResponse<any>>({
      url: `admin/baseDepts/deptUsers/${deptCode}/userId/${userId}`,
      method: "delete",
    });
  }
}

export default DeptAPI;

/** 翻页查询条件 */
export interface TableRequestParams extends TableRequestPage {
  /** 部门名称 */
  deptName?: string;
}
/** 部门信息 */
export interface DeptVO extends BaseVO {
  id?: string;
  /** 父级部门id */
  parentId?: string;
  /** 部门名称 */
  deptName?: string;
  /** 部门编码 */
  deptCode?: string;
  /** 部门说明 */
  description?: string;
}
/** 用户信息 */
export interface DeptUserVO {
  /** 用户ids */
  userIds: string[];
}
