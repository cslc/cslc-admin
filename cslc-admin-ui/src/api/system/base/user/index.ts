import request from "@/utils/request";

class UserAPI {
  /**
   * 查询列表
   * @param {*} query 查询条件
   */
  static getTableDataApi(params: TableRequestParams) {
    return request<any, TableResultResponse<UserVO>>({
      url: "/admin/baseUsers",
      method: "get",
      params,
    });
  }
  /**
   * 添加新实体
   * @param {*} obj 实体信息
   */
  static addBaseUserApi(obj: UserVO) {
    return request<any, ObjectResultResponse<UserVO>>({
      url: "/admin/baseUsers",
      method: "post",
      data: obj,
    });
  }
  /**
   * 通过获取用户基本信息
   */
  static getBaseUserApi() {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/baseUsers/info",
      method: "get",
    });
  }
  /**
   * 通过主键删除实体
   * @param {*} id 主键
   */
  static delBaseUserApi(id?: string) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/baseUsers/" + id,
      method: "delete",
    });
  }
  /**
   * 通过id更新实体
   * @param {*} id 主键
   * @param {*} obj 实体信息
   */
  static putBaseUserApi(id: string, obj: UserVO) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/baseUsers/" + id,
      method: "put",
      data: obj,
    });
  }
  /**
   * 修改用户密码
   * @param {JSON} obj 用户密码
   */
  static changePasswordApi(obj: any) {
    return request<any, ObjectResultResponse<any>>({
      url: "/admin/baseUsers/password",
      method: "put",
      data: obj,
    });
  }

  /**
   * 通过部门编码获取用户列表
   *
   * @param {String} deptCode 部门编码
   * @param {*} query 查询条件
   *
   */
  static getDeptUsersByDeptCodeApi(
    deptCode: string,
    query: DeptUserTableRequestParams
  ) {
    return request<any, TableResultResponse<UserVO>>({
      url: `/admin/baseUsers/deptCode/${deptCode}`,
      method: "get",
      params: query,
    });
  }
  /**
   * 通过用户主键获取用户信息
   */
  static getBaseUserByUserIdApi(userId: string) {
    return request<any, ObjectResultResponse<UserVO>>({
      url: "/admin/baseUsers/" + userId,
      method: "get",
    });
  }
  /**
   * 获取用户角色编码列表
   * @param {String} userId 用户id
   */
  static getUserRolesByUserIdApi(userId: string) {
    return request<any, ObjectResultResponse<any>>({
      url: `/admin/baseUsers/${userId}/role`,
      method: "get",
    });
  }

  /**
   * 获取用户角色编码列表
   * @param {String} userId 用户id
   * @param {Array} roleCodes 角色编码集
   */
  static setUserRoleApi(userId: string, roleCodes: any) {
    return request<any, ObjectResultResponse<any>>({
      url: `/admin/baseUsers/${userId}/role`,
      method: "put",
      data: roleCodes,
    });
  }
  /**
   * 排除指定部门的用户列表
   *
   * @param {String} excludeDeptCode 排除部门
   * @param {*} query 查询条件
   *
   */
  static getUsersExcludeDeptApi(excludeDeptCode: string, query: any) {
    return request<any, ObjectResultResponse<any>>({
      url: `/admin/baseUsers/exclude/${excludeDeptCode}`,
      method: "get",
      params: query,
    });
  }
}

export default UserAPI;

/** 翻页查询条件 */
export interface TableRequestParams extends TableRequestPage {
  /** 查询参数：用户名 */
  userName?: string;
}
/** 部门用户查询条件 */
export interface DeptUserTableRequestParams extends TableRequestPage {
}
/** 用户对象 */
export interface UserVO extends BaseVO {
  id?: string;
  account?: string;
  password?: string;
  userName?: string;
  userSex?: number;
  isDisabled?: number;
  mobilePhone?: string;
  userEmail?: string;
  description?: string;
  tenantId?: string;
}
