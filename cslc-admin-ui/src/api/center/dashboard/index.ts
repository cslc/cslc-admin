import request from "@/utils/request";

class DashboardAPI {
  /**
   * 仪表盘基础统计
   */
  static getBaseVisitStatsApi() {
    return request<any, ObjectResultResponse<VisitStatsVO[]>>({
      url: "/statistics/dashboard/base",
      method: "get",
    });
  }
  /**
   * 消费趋势图
   */
  static getConsumeVisitStatsApi() {
    return request<any, ObjectResultResponse<any>>({
      url: "/statistics/consume",
      method: "get",
    });
  }
}
export default DashboardAPI;

/**  访问统计 */
export interface VisitStatsVO {
  /** 标题 */
  title: string;
  /** 类型 */
  type: "user" | "orders" | "pay" | "topUp";
  /** 今日访问量 */
  todayCount: number;
  /** 总访问量 */
  totalCount: number;
  /** 单位 */
  granularity: string;
}

/**  访问趋势视图对象 */
export interface VisitTrendVO {
  /** 日期列表 */
  dates: string[];
  /** 总消费 */
  totalList: number[];
  /** 会员消费 */
  memberList: number[];
  /** 散客消费 */
  tempList: number[];
}
