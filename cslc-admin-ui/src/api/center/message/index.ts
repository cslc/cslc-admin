import request from "@/utils/request";

class MessageAPI {
  /**
   * 查询列表
   * @param {*} query 查询条件
   */
  static getBaseMessageListApi(query: TableRequestParams) {
    return request<any, TableResultResponse<MessageVO>>({
      url: "/message/page",
      method: "get",
      params: query,
    });
  }
  /**
   * 通过主键获取实体详情
   * @param {*} id 主键
   */
  static getBaseMessageApi(id: number) {
    return request<any, ObjectResultResponse<MessageVO>>({
      url: "/message/" + id,
      method: "get",
    });
  }
}
export default MessageAPI;

/** 翻页查询条件 */
export interface TableRequestParams extends TableRequestPage {
  /** 查询参数：标题 */
  title?: string;
  /** 消息类型 */
  type?: string;
}

export interface MessageVO extends BaseVO {
  id?: number;
  /** 消息标题 */
  title?: string;
  /** 消息类型 */
  type?: string;
  /** 消息内容 */
  content?: string;
}
