import request from "@/utils/request";

class AuthAPI {
  /** 登录 接口*/
  static login(data: LoginData) {
    // base64 转码
    let dataTemp = { ...data };
    dataTemp.password = btoa(dataTemp.password);
    return request<any, ObjectResultResponse<LoginResult>>({
      url: "/auth/login",
      method: "post",
      data: dataTemp,
    });
  }

  /** 注销 接口*/
  static logout() {
    return request({
      url: "/auth/logout",
      method: "post",
    });
  }

  /** 获取验证码 接口*/
  static getCaptcha() {
    return request<any, ObjectResultResponse<CaptchaResult>>({
      url: "/auth/login/verifyCode",
      method: "get",
    });
  }
}

export default AuthAPI;

/** 登录请求参数 */
export interface LoginData {
  /** 用户名 */
  account: string;
  /** 密码 */
  password: string;
  /** 验证码缓存key */
  token: string;
  /** 验证码 */
  code: string;
}

/** 登录响应 */
export interface LoginResult {
  /** 访问token */
  token?: any;
}

/** 验证码响应 */
export interface CaptchaResult {
  /** 验证码缓存key */
  token: string;
  /** 验证码图片Base64字符串 */
  image: string;
}
