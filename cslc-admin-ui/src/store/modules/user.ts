import AuthAPI from "@/api/auth";
import UserAPI from "@/api/user";
import { resetRouter } from "@/router";
import { store } from "@/store";

import type { LoginData } from "@/api/auth";
import type { UserInfo } from "@/api/user";
import { TOKEN_KEY, MENU_TREE_KEY, MENU_BUTTON_KEY } from "@/enums/CacheEnum";

export const useUserStore = defineStore("user", () => {
  const user = ref<UserInfo>({
    baseUser: {
      id: "",
      account: "",
      userName: "",
    },
  });

  /**
   * 登录
   *
   * @param {LoginData}
   * @returns
   */
  function login(loginData: LoginData) {
    return new Promise<void>((resolve, reject) => {
      AuthAPI.login(loginData)
        .then((res) => {
          const { data } = res;
          localStorage.setItem(TOKEN_KEY, data.token || "");
          resolve();
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  // 获取信息(用户昵称、头像、角色集合、权限集合)
  function getUserInfo() {
    return new Promise<UserInfo>((resolve, reject) => {
      UserAPI.getInfo()
        .then((res) => {
          const { data } = res;
          if (!data) {
            reject("验证失败，请重新登录！");
            return;
          }
          Object.assign(user.value, { ...data });
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  // 退出
  function logout() {
    return new Promise<void>((resolve, reject) => {
      AuthAPI.logout()
        .then(() => {
          // 清空token
          localStorage.removeItem(TOKEN_KEY);

          // 清空菜单
          localStorage.removeItem(MENU_TREE_KEY);
          localStorage.removeItem(MENU_BUTTON_KEY);

          location.reload(); // 清空路由
          resolve();
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  // remove token
  function resetToken() {
    return new Promise<void>((resolve) => {
      localStorage.removeItem(TOKEN_KEY);
      resetRouter();
      resolve();
    });
  }

  return {
    user,
    login,
    getUserInfo,
    logout,
    resetToken,
  };
});

// 非setup
export function useUserStoreHook() {
  return useUserStore(store);
}
