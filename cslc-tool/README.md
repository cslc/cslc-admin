# 不同场景的配置

### 1.本地文件服务相关配置

```yml
oss:
    type: local
    localStoragePathPrefix: ${LOCAL_STORAGE_PATH:C://temp_file}
    httpUrl: ${OSS_HTTP_URL:oss_http_url}
```

### 2.阿里云文件服务相关配置

```yml
oss:
  type: aliyun
  # 阿里云绑定的域名
  aliyunDomain:
  # 阿里云路径前缀
  aliyunPrefix:
  #阿里云BucketName
  aliyunBucketName:
  # 阿里云EndPoint
  aliyunEndPoint:
  # 阿里云AccessKeyId
  aliyunAccessKeyId:
  # 阿里云AccessKeySecret
  aliyunAccessKeySecret: 
```