/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.tool.oss.cloud;

import com.cs.cslc.tool.oss.config.CloudStorageConfig;
import com.cs.cslc.tool.oss.util.FileUtil;
import com.cs.cslc.tool.oss.vo.FileVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * LocalStorageService 本地存储.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2021-06-11 14:02
 */
@Slf4j
public class LocalStorageService extends CloudStorageService {

    public LocalStorageService(CloudStorageConfig config) {
        this.config = config;
    }

    @Override
    public String upload(byte[] data, String path) {
        return null;
    }

    @Override
    public String uploadSuffix(byte[] data, String suffix) {
        return null;
    }

    @Override
    public FileVO uploadFileVO(byte[] data, String suffix) {
        return null;
    }

    @Override
    public String upload(InputStream inputStream, String path) {
        return null;
    }

    @Override
    public String uploadSuffix(InputStream inputStream, String suffix) {
        return null;
    }

    @Override
    public boolean remove(String key) {
        return true;
    }

    /**
     * 保存本地文件
     *
     * @param multipartFile 上传文件对象
     * @return prefix 本地存储前缀
     * 格式：xxx/xxxx.xxx(E:/data/text.txt)
     */
    public static FileVO saveLocalStorage(MultipartFile multipartFile, String prefix) {
        // 文件名称
        String filename = multipartFile.getOriginalFilename();
        if (StringUtils.isBlank(filename)) {
            filename = "";
        }
        // 文件扩展名
        String suffix = filename.substring(filename.lastIndexOf("."));
        String fileName = filename.substring(0, filename.lastIndexOf("."));

        // 生成sign
        String sign = UUID.randomUUID().toString().replace("-", "");
        String filePath = FileUtil.getPath(suffix, sign);
        try {
            File file = new File(prefix + filePath);
            File fileParent = file.getParentFile();
            // 判断目录/文件，没有则创建
            if (!fileParent.exists()) {
                fileParent.mkdirs();
            }
            multipartFile.transferTo(file);

        } catch (IOException e) {
            log.error("saveFile file fail,  LocalStorage throw error.", e);
            return null;
        }

        return FileVO.builder()
                .fileName(fileName)
                .suffix(suffix)
//                .sign(Base64.getUrlEncoder().encodeToString(filePath.getBytes()))
                .sign(filePath)
                .size(multipartFile.getSize())
                .build();
    }
}
