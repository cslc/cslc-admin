/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.tool.oss.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

/**
 * CloudStorageConfig 存储配置属性.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2021-06-11 14:02
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "oss")
public class CloudStorageConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 存储类型，参考常量类：OSSConstant
     */
    private String type;

    /**
     * 七牛绑定的域名
     */
    private String qiniuDomain;
    /**
     * 七牛路径前缀
     */
    private String qiniuPrefix;
    /**
     * 七牛ACCESS_KEY
     */
    private String qiniuAccessKey;
    /**
     * 七牛SECRET_KEY
     */
    private String qiniuSecretKey;
    /**
     * 七牛存储空间名
     */
    private String qiniuBucketName;

    /**
     * 阿里云绑定的域名
     */
    private String aliyunDomain;
    /**
     * 阿里云路径前缀
     */
    private String aliyunPrefix;
    /**
     * 阿里云EndPoint
     */
    private String aliyunEndPoint;
    /**
     * 阿里云AccessKeyId
     */
    private String aliyunAccessKeyId;
    /**
     * 阿里云AccessKeySecret
     */
    private String aliyunAccessKeySecret;
    /**
     * 阿里云BucketName
     */
    private String aliyunBucketName;

    /**
     * 腾讯云绑定的域名
     */
    private String qcloudDomain;
    /**
     * 腾讯云路径前缀
     */
    private String qcloudPrefix;
    /**
     * 腾讯云AppId
     */
    private Integer qcloudAppId;
    /**
     * 腾讯云SecretId
     */
    private String qcloudSecretId;
    /**
     * 腾讯云SecretKey
     */
    private String qcloudSecretKey;
    /**
     * 腾讯云BucketName
     */
    private String qcloudBucketName;
    /**
     * 腾讯云COS所属地区
     */
    private String qcloudRegion;

    /**
     * local存储配置前缀
     */
    private String localStoragePathPrefix;

}
