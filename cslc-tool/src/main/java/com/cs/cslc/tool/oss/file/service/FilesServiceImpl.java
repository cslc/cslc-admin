package com.cs.cslc.tool.oss.file.service;

import com.cs.cslc.common.biz.BaseBusinessBiz;
import com.cs.cslc.tool.oss.file.entity.Files;
import com.cs.cslc.tool.oss.file.mapper.FilesMapper;
import com.cs.cslc.tool.oss.vo.FileVO;
import org.springframework.stereotype.Service;

/**
 * Files 文件信息表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-04-07 09:46
 */
@Service
public class FilesServiceImpl extends BaseBusinessBiz<FilesMapper, Files> {
    /**
     * 保存文件
     */
    public Boolean saveFile(FileVO fileVO) {
        // 判断是否存在，存在则不保存
        Long cont = this.selectCount(Files.builder().sign(fileVO.getSign()).build());
        if (cont == 0) {
            return this.insertModel(Files.builder()
                    .sign(fileVO.getSign())
                    .filename(fileVO.getFileName())
                    .size(String.valueOf(fileVO.getSize()))
                    .suffix(fileVO.getSuffix())
                    .build());
        }
        return true;
    }
}