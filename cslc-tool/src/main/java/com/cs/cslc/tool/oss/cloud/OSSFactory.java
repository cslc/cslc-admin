/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.tool.oss.cloud;

import com.cs.cslc.tool.oss.config.CloudStorageConfig;
import com.cs.cslc.tool.oss.constants.OSSConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * OSSFactory 文件上传Factory.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2021-06-11 14:02
 */
@Component
public class OSSFactory {

    @Autowired
    private CloudStorageConfig config;

    public CloudStorageService build() {
        CloudStorageService result;
        // 构造存储类型类型
        String type = config.getType();
        switch (type) {
            case OSSConstant.QINIU_TYPE:
                result = new QiniuCloudStorageService(config);
                break;
            case OSSConstant.ALIYUN_TYPE:
                result = new AliyunCloudStorageService(config);
                break;
            case OSSConstant.QCLOUD_TYPE:
                result = new QcloudCloudStorageService(config);
                break;
            default:
                result = new LocalStorageService(config);
        }
        return result;
    }

}
