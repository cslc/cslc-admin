package com.cs.cslc.tool.oss.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

/**
 * FileVO 文件VO.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-07-15
 */
@Data
@Builder
@Schema(description = "文件VO")
public class FileVO {

    @Schema(description = "文件唯一标志/文件路径")
    private String sign;

    @Schema(description = "文件名")
    private String fileName;

    @Schema(description = "扩展名")
    private String suffix;

    @Schema(description = "文件大小（单位:KB）")
    private Long size;

}
