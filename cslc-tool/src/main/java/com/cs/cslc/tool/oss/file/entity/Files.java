package com.cs.cslc.tool.oss.file.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * Files 文件信息表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-04-07 09:46
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("base_file")
public class Files implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @Schema(description = "id")
    @TableId(type = IdType.AUTO)
    private Integer id;

    @Schema(description = "文件唯一标志")
    @TableField("sign")
    private String sign;

    @Schema(description = "文件名")
    @TableField("file_name")
    private String filename;

    @Schema(description = "扩展名")
    @TableField("suffix")
    private String suffix;

    @Schema(description = "文件大小（单位:KB）")
    @TableField("size")
    private String size;

    @Schema(description = "创建日期")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}