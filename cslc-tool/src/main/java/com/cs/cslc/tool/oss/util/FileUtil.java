/*
 *MIT License
 *
 *Copyright (c) 2019 chenshuai cs4380@163.com
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */

package com.cs.cslc.tool.oss.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Calendar;
import java.util.UUID;

/**
 * FilePathUtil 文件工具类.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-07-15
 */
public class FileUtil {

    /**
     * 通过日期生成文件路径
     *
     * @return 格式：/yyyy/mm/dd/hh/
     */
    public static String datePath() {
        Calendar calendar = Calendar.getInstance();
        StringBuilder datePath = new StringBuilder();
        datePath.append("/").append(calendar.get(Calendar.YEAR))
                .append("/").append(calendar.get(Calendar.MONTH) + 1)
                .append("/").append(calendar.get(Calendar.DAY_OF_MONTH)).append("/");
        return datePath.toString();
    }

    /**
     * 文件路径
     *
     * @param suffix 后缀
     *               格式： .xxx (.pdf|.ppt|.png)
     * @return 返回上传路径
     * 格式: /yyyy/mm/dd/hh/uuid.${suffix}
     */
    public static String getPath(String suffix) {
        // 生成uuid
        String uuid = UUID.randomUUID().toString().replace("-", "");
        return getPath(suffix, uuid);
    }

    /**
     * 文件路径
     *
     * @param suffix 后缀
     *               格式： .xxx (.pdf|.ppt|.png)
     * @return 返回上传路径
     * 格式: /yyyy/mm/dd/hh/uuid.${suffix}
     */
    public static String getPath(String suffix, String uuid) {
        // 文件路径
        return datePath() + uuid + suffix;
    }

    /**
     * 下载文件
     *
     * @param file 下载的文件
     * @return 文件流
     * @throws IOException 异常
     */
    public static ResponseEntity<byte[]> download(File file) throws IOException {
        //设置响应头
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment;filename=" + file.getName());
        headers.add("Content-Type", "application/octet-stream");
        return new ResponseEntity<>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
    }

    /**
     * 获取本地临时文件
     *
     * @param prefix   文件路径前缀   格式：xx/xx (E:/temp | /data/temp)
     * @param filePath 文件路径（baser64编码）
     * @return 本地临时文件
     */
    public static File getLocalStorageFile(String prefix, String filePath) {
        if (StringUtils.isBlank(prefix) || StringUtils.isBlank(filePath)) {
            return null;
        }
        byte[] filePaths = Base64.getUrlDecoder().decode(filePath);
        filePath = prefix + new String(filePaths);
        return new File(filePath);
    }
}