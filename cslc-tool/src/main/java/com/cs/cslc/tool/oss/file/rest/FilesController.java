package com.cs.cslc.tool.oss.file.rest;

import com.cs.cslc.common.rest.BaseController;
import com.cs.cslc.tool.oss.file.entity.Files;
import com.cs.cslc.tool.oss.file.service.FilesServiceImpl;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Files 文件信息表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-04-07 09:46
 */
@RestController
@RequestMapping("/base/file")
@Tag(name = "文件信息表服务")
public class FilesController extends BaseController<FilesServiceImpl, Files> {

}