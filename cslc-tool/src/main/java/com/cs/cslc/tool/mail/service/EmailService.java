package com.cs.cslc.tool.mail.service;


/**
 * EmailService 邮件服务.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2023-10-10
 */
public interface EmailService {
    /**
     * 发送纯文本邮件
     *
     * @param to      收件人
     * @param subject 主题 / 邮件标题
     * @param content 邮件内容
     */
    void sendSimpleMail(String[] to, String subject, String content);

    /**
     * 发送html邮件
     *
     * @param to      收件人
     * @param subject 主题 / 邮件标题
     * @param content 邮件内容
     */
    void sendHtmlMail(String[] to, String subject, String content);

    /**
     * 发送携带附件的邮件
     *
     * @param to       收件人
     * @param subject  主题 / 邮件标题
     * @param content  邮件内容
     * @param filePath 附件路径
     */
    void sendAttachmentsMail(String[] to, String subject, String content, String filePath);
}
