package com.cs.cslc.tool.oss.file.mapper;

import com.cs.cslc.common.mapper.SuperMapper;
import com.cs.cslc.tool.oss.file.entity.Files;

/**
 * Files 文件信息表.
 *
 * @author cs4380 https://gitee.com/xhbug_cs4380  cs4380@163.com
 * @version 1.0
 * @since 2024-04-07 09:46
 */
public interface FilesMapper extends SuperMapper<Files> {

}
